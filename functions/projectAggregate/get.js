const { authenticate } = require("../lib/auth");
const getDataById = require("../lib/functions/getDataById");
const getProjectPermissions = require('../lib/functions/getProjectRestrictions');

module.exports = authenticate(async function (context, req) {
  return getDataById(context, req, "project_aggregate", getProjectPermissions, null, true);
});