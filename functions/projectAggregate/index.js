const methodDelegator = require("../lib/functions/methodDelegator");

const GET = require("./get");

module.exports = methodDelegator({
  GET
});