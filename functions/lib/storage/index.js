const {
  Aborter,
  BlobURL,
  BlockBlobURL,
  ContainerURL,
  ServiceURL,
  StorageURL,
  SharedKeyCredential,
  uploadStreamToBlockBlob
} = require("@azure/storage-blob");

const STORAGE_ACCOUNT_NAME = process.env.AZURE_STORAGE_ACCOUNT_NAME;
const ACCOUNT_ACCESS_KEY = process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY
const ONE_MEGABYTE = 1024 * 1024;
const FOUR_MEGABYTES = 4 * ONE_MEGABYTE;
const ONE_MINUTE = 60 * 1000;

module.exports = class {
  constructor(containerName) {
    const credentials = new SharedKeyCredential(STORAGE_ACCOUNT_NAME, ACCOUNT_ACCESS_KEY);
    const pipeline = StorageURL.newPipeline(credentials);
    const serviceURL = new ServiceURL(`https://${STORAGE_ACCOUNT_NAME}.blob.core.windows.net`, pipeline);
    this._containerURL = ContainerURL.fromServiceURL(serviceURL, containerName);
  }

  async uploadStream(stream, blobName) {
    const aborter = Aborter.timeout(30 * ONE_MINUTE);
    const blockBlobURL = BlockBlobURL.fromContainerURL(this._containerURL, blobName);

    const uploadOptions = {
        bufferSize: FOUR_MEGABYTES,
        maxBuffers: 5,
    };

    return await uploadStreamToBlockBlob(
      aborter, 
      stream, 
      blockBlobURL, 
      uploadOptions.bufferSize, 
      uploadOptions.maxBuffers);
  }

  async getBlob(blobName) {
    const blobURL = BlobURL.fromContainerURL(this._containerURL, blobName);
    return await blobURL.download(Aborter.none, 0);
  }
};