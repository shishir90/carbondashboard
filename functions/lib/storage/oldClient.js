const azure = require('azure-storage');

const STORAGE_ACCOUNT_NAME = process.env.AZURE_STORAGE_ACCOUNT_NAME;
const ACCOUNT_ACCESS_KEY = process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY

module.exports = class {
  constructor(containerName) {
    this._containerName = containerName;
  }

  generateSasToken(blobName, permissions) {
    let container = this._containerName;
    let blobService = azure.createBlobService(STORAGE_ACCOUNT_NAME, ACCOUNT_ACCESS_KEY);

    // Create a SAS token that expires in an hour
    // Set start time to five minutes ago to avoid clock skew.
    let startDate = new Date();
    startDate.setMinutes(startDate.getMinutes() - 5);
    let expiryDate = new Date(startDate);
    expiryDate.setMinutes(startDate.getMinutes() + 60);

    permissions = permissions || azure.BlobUtilities.SharedAccessPermissions.READ;

    let sharedAccessPolicy = {
        AccessPolicy: {
            Permissions: permissions,
            Start: startDate,
            Expiry: expiryDate
        }
    };
    
    let sasToken = blobService.generateSharedAccessSignature(container, blobName, sharedAccessPolicy);
    
    return {
        token: sasToken,
        uri: blobService.getUrl(container, blobName, sasToken, true)
    };
  }
};