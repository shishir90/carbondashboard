module.exports = origin => {
  if (process.env.ALLOWED_ORIGINS.split(",").indexOf(origin) >= 0) {
    return origin;
  } else {
    return "";
  }
}