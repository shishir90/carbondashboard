const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();

const FLIGHT_MANDATORY_FIELDS = [
    new Field("Departure Date", "departureDate", dateParse),
    new Field("Source Name", "flightType"),
    new Field("Standard Usage", "distance", parseFloat),
    new Field("Standard Usage", "flightDistance", parseFloat),
    new Field("Flight Class", "flightClass"),
    new Field("Ticket No.", "ticketNumber"),
];

const FLIGHT_OPTIONAL_FIELDS = [
    new Field("Carrier Name", "flightCarrier"),
    new Field("Flight Route", "flightRoute"),
    new Field("From Airport", "originAirport"),
    new Field("To Airport", "destinationAirport"),
    new Field("From Country", "originCountry"),
    new Field("To Country", "destinationCountry"),
    new Field("From City", "originCity"),
    new Field("To City", "destinationCity"),
    new Field("Description/Notes", "description")
];

module.exports = class Flight extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return FLIGHT_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return FLIGHT_OPTIONAL_FIELDS;
    }
}