const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();
const intParse = (intValue, inflation) => inflation ? intValue : parseInt(intValue);

const HOTEL_MANDATORY_FIELDS = [
    new Field("Source Name Calc", "type"),
    new Field("Check-In Date", "checkIn", dateParse),
    new Field("Check-In Date", "departureDate", dateParse),
    new Field("Check-Out Date", "checkOut", dateParse),
    new Field("Standard Usage", "nights", intParse),
    new Field("Employee Project Code", "projectCode")
];

const HOTEL_OPTIONAL_FIELDS = [
    new Field("Country of Hotel Stay", "hotelCountry"),
    new Field("Discipline", "discipline"),
    new Field("Text 3", "hotelDescription")
];

module.exports = class Hotel extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return HOTEL_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return HOTEL_OPTIONAL_FIELDS;
    }
    static validateObject(object) {
        return (object["Source Name Calc"] && object["Source Name Calc"].toLowerCase().indexOf("hotel") >= 0) && this.MANDATORY_FIELDS.reduce((result, field) => result && !!object[field.csvFieldName], true);
    }
}