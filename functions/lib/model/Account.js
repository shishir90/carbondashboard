const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const ACCOUNT_MANDATORY_FIELDS = [
  new Field("Project Customer Number","_id"),
  new Field("Project Customer Name","name"),
  new Field("Project Customer Desc","desc"),
  new Field("Project Customer Category Desc","categoryDesc"),
  new Field("Production Unit Code","productionUnitCode"),
  new Field("Organization Name","organizationName"),
  new Field("Project Operating Unit Name","projectOperatingUnitName"),
  new Field("Sector Code","sectorCode"),
  new Field("Sector Name","sectorName"),
  new Field("Sub Sector Code","subSectorCode"),
  new Field("Sub Sector Name","subSectorName"),
  new Field("Bu Code","buCode"),
  new Field("Bu Name","buName"),
  new Field("Sbu Code","sbuCode"),
  new Field("Sbu Name","sbuName"),
  new Field("Discipline Code","disciplineCode"),
  new Field("Discipline Name","disciplineName")
];

const ACCOUNT_OPTIONAL_FIELDS = [
  new Field("Project Final Customer Number","projectFinalCustomerNumber"),
  new Field("Project Final Customer Name","projectFinalCustomerName"),
  new Field("Project Final Customer Type Desc","projectFinalCustomerTypeDesc"),
  new Field("Project Final Customer Category Desc","projectFinalCustomerCategoryDesc"),
  new Field("CMA Code","cmaCode"),
  new Field("CMA Name","cmaName")
];

module.exports = class Account extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return ACCOUNT_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return ACCOUNT_OPTIONAL_FIELDS;
    }
    static get ALL_FIELDS() {
      return [].concat(ACCOUNT_MANDATORY_FIELDS, ACCOUNT_OPTIONAL_FIELDS);
    }
};