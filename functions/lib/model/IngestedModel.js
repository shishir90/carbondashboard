const { MANDATORY_FIELDS, OPTIONAL_FIELDS } = require("./CommonFields");
const IncorrectModelError = require("../error/IncorrectModelError");

module.exports = class IngestedModel {
    constructor(parsedCSV) {
        if (parsedCSV === false) {
            // all good, ignore
        } else if (this.constructor.validateObject(parsedCSV)) {
            Object.assign(this, this.constructor.ALL_FIELDS.reduce((result, field) => Object.assign({}, result, {
                [field.modelFieldName]: field.resolveValue(parsedCSV)
            }), {
                _type: this.constructor.name
            }));
        } else {
            throw new IncorrectModelError(this.constructor, parsedCSV);
        }
    }
    inflate(json) {
        Object.assign(this, this.constructor.ALL_FIELDS.reduce((result, field) => Object.assign({}, result, {
            [field.modelFieldName]: field.transformFunction ? field.transformFunction(json[field.modelFieldName], true) : json[field.modelFieldName]
        }), {
            _type: this.constructor.name
        }));
    }
    static get ALL_FIELDS() {
        return [].concat(this.MANDATORY_FIELDS, MANDATORY_FIELDS, this.OPTIONAL_FIELDS, OPTIONAL_FIELDS);
    }
    static validateObject(object) {
        return this.MANDATORY_FIELDS.reduce((result, field) => {
            return result && !!field.resolveValue(object);
        }, true);
    }
};