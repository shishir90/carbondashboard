const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();

const TAXI_MANDATORY_FIELDS = [
    new Field("Source Name", "type"),
    new Field("Departure Date", "departureDate", dateParse),
    new Field("Standard Usage", "distance", parseFloat),
];

const TAXI_OPTIONAL_FIELDS = [
    new Field("Expense ID", "expenseID"),
    new Field("Description/Notes", "description")
];

const TAXI_SOURCES = [
    "Taxi"
].map(s => s.toLowerCase())

module.exports = class Taxi extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return TAXI_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return TAXI_OPTIONAL_FIELDS;
    }
    static validateObject(object) {
        return object["Source Name"] && TAXI_SOURCES.indexOf(object["Source Name"].toLowerCase()) >= 0 && this.MANDATORY_FIELDS.reduce((result, field) => result && !!object[field.csvFieldName], true);
    }
}