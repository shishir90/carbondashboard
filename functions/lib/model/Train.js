const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : dateValue && moment(dateValue, "D-MMM-YY").toDate();

const TRAIN_MANDATORY_FIELDS = [
    new Field("Source Name", "type"),
    new Field("Departure Date", "departureDate", dateParse),
    new Field("Standard Usage", "distance", parseFloat),
];

const TRAIN_OPTIONAL_FIELDS = [
    new Field("Arrival Date", "arrivalDate", dateParse),
    new Field("Expense ID", "expenseID"),
    new Field("Description/Notes", "description"),
    new Field("Origin Location", "origin"),
    new Field("Destination Location", "destination"),
    new Field("Description/Notes", "isReturn", value => value === true || Boolean(value && value.toLowerCase && value.toLowerCase().indexOf("return") >= 0)),
    new Field("Text 1", "travelReason"),
];

const TRAIN_SOURCES = [
    "Domestic Rail",
    "Euro Star",
    "International Rail"
]

module.exports = class Train extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return TRAIN_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return TRAIN_OPTIONAL_FIELDS;
    }
    static validateObject(object) {
        return TRAIN_SOURCES.indexOf(object["Source Name"]) >= 0 && this.MANDATORY_FIELDS.reduce((result, field) => result && !!object[field.csvFieldName], true);
    }
}