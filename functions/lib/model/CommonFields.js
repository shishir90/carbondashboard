const moment = require("moment");
const Field = require("./Field");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();
const floatParse = (floatValue, inflation) => inflation ? floatValue : parseFloat(floatValue);

const MANDATORY_FIELDS = [
    new Field("ID", "_id"), 
    new Field("Cost", "cost", floatParse),
    new Field("Currency", "currency"),
    new Field("Cost in EUR", "euroCost", floatParse),
    new Field("Accounting Date", "accountingDate", dateParse),
    new Field("Text 1", "expenseType"),
    new Field("Text 2", "incurredLocation"),
    new Field("Emissions Generated", "emissionsKG", floatParse),
    new Field("SBU", "sbu"),
    new Field("Employee Project Code", "projectCode")
];

const OPTIONAL_FIELDS = [
    new Field("Discipline", "discipline"),
    new Field("Employee ID", "employeeId"),
    new Field("Local Currency", "incurredCurrency"),
    new Field("Cost in Local Currency", "incurredCost", floatParse)
];

module.exports = { MANDATORY_FIELDS, OPTIONAL_FIELDS };