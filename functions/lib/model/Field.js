const flat = require('array-flatten');

module.exports = class Field {

    constructor(csvFieldName, modelFieldName, transformFunction) {
        this.csvFieldName = csvFieldName;
        this.modelFieldName = modelFieldName;
        this.transformFunction = transformFunction;
    }

  resolveValue (object) {
    if(!!object) {
      if (!this.transformFunction) {
        return this.getResolveValue(object);
      } else {
        return this.transformFunction(this.getResolveValue(object));
      }
    }
  }

  getResolveValue (object) {
    return flat(this.csvFieldName.split(".").map(s => s.split(":"))).reduce((result, pathPart) => result[pathPart], object);
  }
};