const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();

const CAR_MANDATORY_FIELDS = [
    new Field("Source Name", "type"),
    new Field("Departure Date", "departureDate", dateParse),
    new Field("Standard Usage", "distance", parseFloat),
];

const CAR_OPTIONAL_FIELDS = [
    new Field("Expense ID", "expenseID"),
    new Field("Description/Notes", "description"),
    new Field("Origin Location", "origin"),
    new Field("Destination Location", "destination"),
    new Field("Travel Type", "travelType"),
    new Field("Text 1", "travelReason")

];

const CAR_SOURCES = [
    "Average Car",
    "Car Rental Cost",
    "Diesel (Biofuel blend)",
    "Diesel (Vehicle)",
    "Motorbike",
    "Petrol (Vehicle)",
    "Bus"
].map(s => s.toLowerCase())

module.exports = class Car extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return CAR_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return CAR_OPTIONAL_FIELDS;
    }
    static validateObject(object) {
        return (CAR_SOURCES.indexOf(object["Source Name"]) >= 0 ||
            (object["Source Name"] && object["Source Name"].toLowerCase().indexOf("car") >= 0) ) && this.MANDATORY_FIELDS.reduce((result, field) => result && !!object[field.csvFieldName], true);
    }
}