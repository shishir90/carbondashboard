const moment = require("moment");
const Field = require("./Field");
const IngestedModel = require("./IngestedModel");

const dateParse = (dateValue, inflation) => inflation ? new Date(Date.parse(dateValue)) : moment(dateValue, "D-MMM-YY").toDate();

const PROJECT_MANDATORY_FIELDS = [
  new Field("Project Number", "_id"),
  new Field("Project Name", "name"),
  new Field("Project Customer Number", "projectCustomerNumber")
];

const PROJECT_OPTIONAL_FIELDS = [
  new Field("Project Description", "description"),
  new Field("Cross chargeable field", "crossChargeableField"),
  new Field("Billable/non-billable flag", "billableFlag"),
  new Field("Payment Term Name", "paymentTermName"),
  new Field("Payment Term Desc", "paymentTermDesc"),
  new Field("Project Start Date", "startDate", dateParse),
  new Field("Project Completion Date", "completionDate", dateParse),
  new Field("Project Closed Date", "closeDate", dateParse),
  new Field("Project Type", "type"),
  new Field("Project Status", "status"),
  new Field("Project Class Code", "classCode"),
  new Field("Project Group Type", "groupType"),
  new Field("Project Functional Currency Code", "functionalCurrencyCode"),
  new Field("Project Invoice Currency Code", "invoiceCurrencyCode"),
  new Field("Project manager name", "managerName"),
  new Field("Project manager Number", "managerNumber"),
  new Field("Project Classifications", "classification"),
  new Field("Project Controller", "controller"),
  new Field("Project Controller Number", "controllerNumber"),
  new Field("Account Manager", "accountManager"),
  new Field("Account Manager Number", "accountManagerNumber"),
  new Field("Expense Approver", "expenseApprover"),
  new Field("Expense Approver Number", "expenseApproverNumber"),
  new Field("Receiver Project Number", "receiverProjectNumber")
];

module.exports = class Project extends IngestedModel {
    static get MANDATORY_FIELDS() {
        return PROJECT_MANDATORY_FIELDS;
    }
    static get OPTIONAL_FIELDS() {
        return PROJECT_OPTIONAL_FIELDS;
    }
    static get ALL_FIELDS() {
      return [].concat(PROJECT_MANDATORY_FIELDS, PROJECT_OPTIONAL_FIELDS);
    }
};