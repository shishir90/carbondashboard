const jwt = require('jsonwebtoken');
const tsAndCs = require('./tsAndCs');

const verify = token => new Promise((resolve, reject) => jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
  if (err) {
    reject(err);
  } else {
    resolve(decoded);
  }
}));

module.exports = handler => function(context, request) {
  if (Boolean(process.env.BYPASS_AUTH) && process.env.BYPASS_AUTH !== "false") {
    if(Boolean(process.env.MOCK_USER)) {
      context.user = require('../../mockuser')
    } else {
      context.user = {}
    }
    return handler(context, request);
  }
  let token = request.headers.authorization && request.headers.authorization.split(" ")[1];
  if (!token) {
    context.status(401);
    context.json({
      reason: "Unauthorized"
    });
  } else {
    return verify(token)
      .then(decodedToken => {
        context.user = decodedToken;
      })
      .then(() => {
        if(tsAndCs(context.user)) {
          handler(context, request)
        } else {
          context.status(412)
            .json({
              reason: "Unaccepted T's & C's"
            });
        }
      })
      .catch(error => {
        context.error("Token Validation Error", error);
        context.status(401);
        context.json({
          reason: "Unauthorized"
        });
      })
  }
};