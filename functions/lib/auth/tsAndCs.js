module.exports = function(user) {
  const threeMonthsAgo = new Date();
  threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
  if(user.tsandcsdate > threeMonthsAgo){
    return true;
  } else {
    return false;
  }
};