const jwt = require('jsonwebtoken');

const sign = body => new Promise((resolve, reject) => jwt.sign(body, process.env.JWT_SECRET, { }, function(err, token) {
  if (err) {
    reject(err);
  } else {
    resolve(token);
  }
}));


module.exports = {
  sign
}