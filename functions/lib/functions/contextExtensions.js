const originsHelper = require("../origins");
module.exports = next => async function(context, req) {
  console.log = context.log;
  context.error = context.log;
  context.status = status => Object.assign(context.res || {}, {
    status
  }) && context;
  context.json = body => Object.assign(context.res || {}, {
    headers: Object.assign(context.res && context.res.headers || {}, {
      "Content-Type": "application/json",
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      "Access-Control-Allow-Origin": originsHelper(req.headers.origin),
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type,Authorization'
    }),
    body: JSON.stringify(body)
  }) && context;
  return await next.apply(null, arguments);
};