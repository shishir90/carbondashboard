const GetData = require("../database/getData");
const setAggregateQuery =  require('./setAggregateQuery');
const mongo = new GetData(process.env.CARBON_GENERIC_CONNECTION_STRING, process.env.CARBON_DATA_DB_NAME);

module.exports = async function (context, req, collection, indexName, getRestrictions, aggregateCollection, onList, skipValidateAggregate) {
  return getRestrictions(context.user)
    .then(restrictions => {

      if(context.bindingData && context.bindingData.id) {
        const query = {
          $and: [
            {[indexName] : context.bindingData.id + "" },
            restrictions ? restrictions : null
          ]};

        if(aggregateCollection) {
          let aggregate = setAggregateQuery(query, aggregateCollection, skipValidateAggregate);
          console.log(JSON.stringify(aggregate));
          return mongo.getDataAggregate(collection, aggregate, onList )
            .then(data => {
              context.status(data.status).json(data.docs);
            })
            .catch(err => {
              context.status(500).json({reason: "Internal Server Error : " + err});
            });
        } else {
          return mongo.getData(collection, query, onList )
            .then(data => {
              context.status(data.status).json(data.docs);
            })
            .catch(err => {
              context.status(500).json({reason: "Internal Server Error : " + err});
            });
        }
      } else  {
        context.status(400).json({
          error: "Bad Request, missing id"
        });
      }

    })
    .catch(error => {
      console.log(error);
      context.status(401).json({
        error: error
      })
    });
};