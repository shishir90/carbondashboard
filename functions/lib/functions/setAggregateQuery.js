module.exports = function(query, aggregateCollection, skipValidateAggregate) {
  let arr = [
    {
      $match: query || {}
    },
    {
      $lookup:
        {
          from: aggregateCollection,
          localField: "_id",
          foreignField: "_id",
          as: "aggregations"
        }
    }
  ];
  if (skipValidateAggregate) {
    return arr;
  }
  return arr.concat(
    { $unwind: '$aggregations' },
    {
      $match: {
        "aggregations._id": {
          $exists: true
        }
      }
    },
    {
      $group:
        {
          _id: '$_id',
          aggregations:
            {
              $push:
                '$aggregations'
            }
        }
    }
  );
};