const getDataByIndex = require("./getDataByIndex");

module.exports = async function (context, req, collection, getRestrictions, aggregateCollection, onList, skipValidateAggregate) {
  console.log("is restriction ", Boolean(getRestrictions));
  return getDataByIndex(context, req, collection, "_id", getRestrictions, aggregateCollection, onList, skipValidateAggregate);
};