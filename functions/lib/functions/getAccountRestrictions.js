module.exports = function(user) {
  return new Promise(function (resolve, reject) {
    if (!user) {
      // no user available
      reject("Unauthorised User - check user setup");
    } else if (Boolean(user.role.permissions && user.role.permissions.accounts && user.role.permissions.accounts.all)) {
      // super user - no restrictions
      resolve({});
    } else if (user.accounts.length > 0) {
      // only restricted by accounts
      resolve({"_id": {$in: user.accounts}})
    } else if (user.projects.length > 0) {
      // only selected projects allowed
      reject("NO enough permissions - only project access")
    } else {
      // no permissions at all - user set up error
      reject("NO permissions - check user setup")
    }
  })
};