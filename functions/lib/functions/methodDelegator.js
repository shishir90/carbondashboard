const contextExtensions = require("./contextExtensions");

module.exports = handlers => contextExtensions(async function (context, req) {
  let handler = handlers[req.method.toUpperCase()];
  if (handler) {
    return await handler(context, req);
  } else {
    context.res = {
      status: 404
    };
  }
});