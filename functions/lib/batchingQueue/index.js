module.exports = class BatchingQueue {
    constructor(threshold, batchHandler) {
        this._threshold = threshold;
        this._batchHandler = batchHandler;
        this._events = [];
        this._firedEventQueue = [];
    }
    enqueue(event) {
        this._events.push(event);
        if (this._events.length >= this._threshold) {
            this.fire();
        }
    }
    _fire() {
    }
    fire() {
        let events = [].concat(this._events);
        this._events = [];
        this._batchHandler(events);
    }
}