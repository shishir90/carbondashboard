const mailClient = require('@sendgrid/mail');

module.exports = function(eSubject, htmlBody, recipient) {
  const emailValidation = /^([a-zA-Z0-9_\-.]+)@(capgemini|sogeti|idean|fahrenheit-212)\.([a-zA-Z]{2,5})$/;
  if(!emailValidation.test(recipient)){
    return Promise.reject("Invalid email address");
  } else  {
    mailClient.setApiKey(process.env.SENDGRID_API_KEY);
    const eFrom = 'noreply@capgemini.com';
    const msg = {
      to: recipient,
      from: eFrom,
      subject: eSubject,
      html: htmlBody
    };
    return mailClient.send(msg);
  }
};

