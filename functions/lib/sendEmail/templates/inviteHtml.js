const htmlBody = "<h2>Invite</h2>" +
  "<p>This is an invitation to join  {{name}} {{type}}, hope you enjoy.</p>" +
  "";

module.exports = function (data) {
  return htmlBody
    .replace("{{name}}", data.name)
    .replace("{{type}}", data.type);
};