module.exports = async function (user) {

  if(!user) {
    return Promise.reject({status: 401, reason: "Unauthorized User"});
  } else if((!user.projects || !user.projects[0]) && (!user.accounts || !user.accounts[0])) {
    return Promise.reject({status: 403, reason: "Insufficient Permissions"});
  }

  let restrictions = (user.projects && user.projects[0])? {"_id" : {$in: user.projects}} : null;
  if(user.accounts && user.accounts[0]) {
    if(restrictions) {
      restrictions = {
        $or: [
          {"_id" : {$in: user.projects}},
          {"account" : {$in: user.accounts}}
        ]
      };
    } else {
     restrictions =  {"account" : {$in: user.accounts}};
    }
  }

  return restrictions;
};