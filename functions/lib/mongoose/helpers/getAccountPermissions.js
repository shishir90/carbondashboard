module.exports = async function (user) {

  if(!user) {
    return Promise.reject({status: 401, reason: "Unauthorized User"});
  } else if(user.role && user.role.permissions.accounts.all) {
    return null; // no restrictions
  } else if(user.accounts[0]) {
    return {"_id" : {$in: user.accounts}};
  }

  return Promise.reject({status: 403, reason: "Insufficient Permissions"});


};