module.exports = function(query) {
  for(i in query) {
    if(i !== "_id") {
      query[i] = new RegExp(query[i], 'i');
    }
  }
  return this.find( query );
};