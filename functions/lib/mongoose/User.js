const { mongoose, metadata } = require("./");
const RequiredString = require("./types/requiredString");
const queryLike = require("./helpers/queryLike");
require("./Role");

const UserSchema = new mongoose.Schema({
  email: RequiredString,
  forename: String,
  surname: String,
  tsandcsdate: Date,
  role: { type: 'ObjectId', ref: 'Role' },
  accounts: [RequiredString],
  projects: [RequiredString]
});

/**
 * Query Helper for finding a user with fussy match
 * 
 * [NOTE] Must be a declared function (not arrow)
 */

UserSchema.query.like = queryLike;

module.exports = metadata.model('User', UserSchema);