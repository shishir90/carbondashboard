const mongoose = require('mongoose');

const metadata = mongoose.createConnection(process.env.CARBON_META_DB_CONNECTION_STRING, { useNewUrlParser: true });
const data = mongoose.createConnection(process.env.CARBON_DATA_DB_CONNECTION_STRING, { useNewUrlParser: true });

module.exports = {
  mongoose,
  metadata,
  data
}