const { mongoose, metadata } = require("./");
const RequiredString = require("./types/requiredString");

const ProjectSchema = new mongoose.Schema({
  name: RequiredString,
  reference : RequiredString,
  account: { type: 'ObjectId', ref: 'Account' }
});

module.exports = metadata.model('Project', ProjectSchema);