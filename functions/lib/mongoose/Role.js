const { mongoose, metadata } = require("./");
const RequiredString = require("./types/requiredString");

const RoleSchema = new mongoose.Schema({
  name: RequiredString,
  permissions: {
    "accounts": {
      "all": Boolean,
      "invite": Boolean
    },
    "projects": {
      "invite": Boolean
    },
    "data": {
      "upload": Boolean
    },
    "roles": {
      "read": Boolean,
      "write": Boolean,
    }
  }
});


module.exports = metadata.model('Role', RoleSchema);