module.exports = async function (context, req, Class, indexKeyName) {

  if (req.body) {
    const indexKey = {
      [indexKeyName]: req.body[indexKeyName]
    };
    if (!(await Class.findOne(indexKey).exec())) {
      const newItem =  new Class(req.body);
      return newItem.save()
        .then(savedItem => {
          context.json({
            document: savedItem
          });
        })
        .catch(e => {
          context.error(e);
          context.status(400).json({
            reason: "Invalid Request"
          });
        });
    } else {
      context.status(400).json({
        reason: "An item of " + Object.getPrototypeOf(new Class()).collection.collectionName + " with the same " + indexKeyName + " already exists."
      });
    }
  } else {
    context.status(400).json({
      reason: "Invalid Request Body"
    });
  }
};