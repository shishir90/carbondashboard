module.exports = async function (context, req, Class, indexKeyName, restrictions) {

  if (req.query) {
    let userArray = [];


    if ((req.query.id && !restrictions) || (req.query.id && restrictions.indexOf(req.query.id) > -1 )) {
      userArray[0] = await Class.findById(req.query.id).exec();
    } else if (req.query[indexKeyName]) {
      const query =  Object.assign(
        {},
        {
          [indexKeyName] : req.query[indexKeyName],
        },
        restrictions ? restrictions : null
      );
      userArray[0] = await Class.findOne(query).exec();
    } else if (req.query["all"]) {
      userArray = await Class.find(restrictions? restrictions : {}).exec();
    } else {
      const query = Object.assign(
        {},
        req.query,
        restrictions ? restrictions : null
      );
      if(typeof Class.find().like === 'function') {
        userArray = await Class.find().like(query);
      } else {
        userArray = await Class.find(query);
      }
    }

    if (userArray.length > 0 && userArray[0]) {
      context.json({
        success: true,
        document: userArray.length === 1 ? userArray[0] : userArray
      });
    } else {
      context.status(404);
      context.json({
        reason: "No items found with that criteria or no enought permissions to see the result set"
      });
    }
  } else {
    context.status(400);
    context.json({
      reason: "Invalid Request Query"
    });
  }
};