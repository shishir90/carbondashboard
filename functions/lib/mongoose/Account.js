const { mongoose, metadata } = require("./");
const RequiredString = require("./types/requiredString");

const AccountSchema = new mongoose.Schema({
  name: RequiredString,
  reference : RequiredString
});

module.exports = metadata.model('Account', AccountSchema);