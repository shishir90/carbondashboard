const DataIngested = require("./custom-events/DataIngested");
const PreAccountAggregation = require("./custom-events/PreAccountAggregation");

module.exports = {
    DataIngested,
    PreAccountAggregation
}