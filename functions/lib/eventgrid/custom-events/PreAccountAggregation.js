const CustomEvent = require("./CustomEvent");

module.exports = class PreAccountAggregation extends CustomEvent {
    constructor() {
        super("account", {});
    }
    static get dataVersion() {
        return "1.0";
    }
    static get endpoint() {
        return process.env.PRE_ACCOUNT_AGGREGATION_EVENT_EP;
    }
    static get key() {
        return process.env.PRE_ACCOUNT_AGGREGATION_EVENT_KEY;
    }
}