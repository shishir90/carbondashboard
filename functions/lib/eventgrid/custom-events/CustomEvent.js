const uuid = require("uuid");
const fetch = require("node-fetch");

module.exports = class CustomEvent {
    constructor(subject, data) {
        this.subject = subject;
        this.data = data;
    }
    fire() {
        let endpoint = this.constructor.endpoint;
        let payload = [
            {
                "id": uuid.v1(),
                "eventType": this.constructor.name,
                "subject": this.subject,
                "eventTime": new Date(Date.now()).toISOString(),
                "data": this.data,
                "dataVersion": this.constructor.dataVersion
            }
        ];
        return fetch(endpoint, {
            method: "POST",
            body: JSON.stringify(payload),
            headers: {
                "content-type": "application/json",
                "aeg-sas-key": this.constructor.key
            }
        })
    }
}