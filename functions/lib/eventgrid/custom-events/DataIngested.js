const CustomEvent = require("./CustomEvent");

module.exports = class DataIngested extends CustomEvent {
    constructor(collectionType, startDate, endDate) {
        super(collectionType, {
            startDate,
            endDate,
            collectionType
        });
    }
    static get dataVersion() {
        return "1.0";
    }
    static get endpoint() {
        return process.env.DATA_INGESTION_EVENT_EP;
    }
    static get key() {
        return process.env.DATA_INGESTION_EVENT_KEY;
    }
}