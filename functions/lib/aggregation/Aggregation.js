module.exports = class Aggregation {
    constructor(fieldName, aggregationStages) {
        this.fieldName = fieldName;
        this.aggregationStages = aggregationStages;
    }
}