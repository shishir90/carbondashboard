const aad = require('azure-ad-jwt');

module.exports = async req => new Promise((resolve, reject) => {
  let header = req.headers.authorization;
  if (header) {
    let jwtToken = header.split(" ")[1];
    aad.verify(jwtToken, null, function(err, result) {
      if (result) {
        resolve(result);
      } else {
        reject(err)
      }
    }); 
  } else {
    reject();
  }
})