const OfficialMongoClient = require('mongodb').MongoClient;

module.exports = class MongoClient {
    constructor(url, dbname) {
        // Create a new MongoClient
        this._connectionPromise = new Promise((resolve, reject) => OfficialMongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
            if (err) {
                reject(err);
            }
            this._db = client.db(dbname);
            resolve(this._db);
        }));
    }
    getDB() {
        return this._db ? Promise.resolve(this._db) : this._connectionPromise;
    }
}