const { Writable } = require('stream');
const BatchingQueue = require("../batchingQueue");
const MongoClient = require("./mongoClient");

const Hotel = require("../model/Hotel");
const Flight = require("../model/Flight");
const Train = require("../model/Train");
const Car = require("../model/Car");
const Project = require("../model/Project");
const Account = require("../model/Account");
const Taxi = require("../model/Taxi");


const MODELS = [Hotel, Flight, Train, Car, Project, Account, Taxi];

module.exports = class DatabaseUpdate extends Writable {
  constructor(options, completedCallback) {
    // Calls the stream.Writable() constructor
    super(options);
    const url = process.env.CARBON_GENERIC_CONNECTION_STRING;
    const DATA_DB = process.env.CARBON_DATA_DB_NAME;
    this._mongoClient = new MongoClient(url, DATA_DB);
    this.queues = {};
    this.completedCallback = completedCallback;
    this.startDate = null;
    this.endDate = null;
  }
  _write(chunk, encoding, callback) {
    let json = JSON.parse(chunk.toString());
    let model = MODELS.find(model => model.name === json._type);
    let queue = this._getQueue(json._type);
    let instance = new model(false);
    instance.inflate(json);
    if (!this.startDate || instance.accountingDate < this.startDate) {
      this.startDate = new Date(instance.accountingDate);
    } 
    if (!this.endDate || instance.accountingDate > this.endDate) {
      this.endDate = new Date(instance.accountingDate);
    }
    queue.enqueue(instance);
    callback();
  }
  _final(callback) {
    console.log("[DB-WRITABLE_STREAM] Recieved final chunk");
    Promise.all(Object.keys(this.queues)
    .map(queueName => {
      console.log("[DB-WRITABLE_STREAM] Triggering fire on queue: " + queueName);
      return this.queues[queueName].fire();
    }))
      .then(() => this.completedCallback && this.completedCallback(Object.keys(this.queues), this.startDate, this.endDate));
    callback();
  }
  _getQueue(name) {
    let collectionName = name.toLowerCase();
    if (this.queues[collectionName]) {
      return this.queues[collectionName];
    } else {
      this.queues[collectionName] = new BatchingQueue(100000, documents => {
        this._mongoClient.getDB()
          .then(db => {
            console.log("[DB-WRITABLE_STREAM] Executing Bulk on collection: " + collectionName);
            let bulk = db.collection(collectionName).initializeOrderedBulkOp();
            documents.forEach(document => bulk.find( { _id: document._id } ).upsert().updateOne(document));
            bulk.execute();
          }).catch(e => {
            console.log(e, collectionName);
            throw e;
          });
      });
      return this._getQueue(collectionName);
    }
  }
};