const MongoClient = require("./mongoClient");

module.exports = class GetData extends MongoClient{

  getDataAggregate (collection, query, onList) {
    return this.getDB()
      .then(db => {
        return db.collection(collection).aggregate(query, { allowDiskUse: true }).toArray()
          .then(docs => {
            if(docs.length > 0 && docs[0]) {
              if(docs.length > 1 || onList ) {
                return { status : 200, docs: docs};
              } else {
                return { status : 200, docs: docs[0]};
              }
            } else {
              return { status: 404, docs : {reason: "No data found"}};
            }
          })
          .catch(err => {
            return {status: 500, docs: {reason: "Internal Server Error : " + err}}
          });
      });
  }


  getData (collection, query, onList) {
    return this.getDB()
      .then(db => {
        return db.collection(collection).find(query).toArray()
          .then(docs => {
            if(docs.length > 0 && docs[0]) {
              if(docs.length > 1 || !onList) {
                return { status : 200, docs: docs};
              } else {
                return { status : 200, docs: docs[0]};
              }
            } else {
              return { status: 404, docs : {reason: "No data found"}};
            }
          })
          .catch(err => {
            return {status: 500, docs: {reason: "Internal Server Error : " + err}}
          });
      });
  }

  getDataById (collection, id) {
    return this.getData(collection,{_id: id });
  }

};

