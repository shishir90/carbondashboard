const { Transform } = require("stream");
const csv = require("csvtojson");

const Hotel = require("../model/Hotel");
const Flight = require("../model/Flight");
const Train = require("../model/Train");
const Car = require("../model/Car");
const Project = require("../model/Project");
const Account = require("../model/Account");
const Taxi = require("../model/Taxi");

const MODELS = [Hotel, Train, Car, Project, Flight, Taxi];
const SUBMODELS = [Account];

// Finds the appropriate class for the data by executing the static validation method on each class until it has a valid one.
const findAppropriateModel = chunk => MODELS.reduce((result, model) => !!result ? result : model.validateObject(chunk) ? model : result, null);
const findAppropriateSubmodel = chunk => SUBMODELS.reduce((result, model) => !!result ? result : model.validateObject(chunk) ? model : result, null);

class streamObjectToModel extends Transform {
    _transform(data, encoding, done) {
        let parsed = JSON.parse(data.toString());
        try {
            let Construct = findAppropriateModel(parsed);
            if (!!Construct) {
                try {
                    let obj = new Construct(parsed);
                    this.push(Buffer.from(JSON.stringify(obj)));
                    Construct = findAppropriateSubmodel(parsed);
                    if(!!Construct) {
                        let obj = new Construct(parsed);
                        this.push(Buffer.from(JSON.stringify(obj)));
                    }
                    done();
                } catch (e) {
                    console.log("[SERIALISE] error ", e);
                    done(e);
                }
            } else {
                done();
            }
        } catch (e ) {
            console.log(e);
        }
    }
}

module.exports = {
    parseCSVFromStream(stream) {
        return stream
            .pipe(csv());
    },
    streamObjectToModel
};