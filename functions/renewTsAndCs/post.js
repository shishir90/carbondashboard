const { authenticate } = require("../lib/auth");
const User = require("../lib/mongoose/User");

module.exports = authenticate(async function (context, req) {
  if(req.body && req.body.email) {
    updatedUser = await User.findOne({email: req.body.email}).exec();
    if (updatedUser) {
      updatedUser.tsandcsdate = new Date();
      return updatedUser.save()
        .then(() => {
          context.json({
            success: true
          });
        })
        .catch(e => {
          context.error(e);
          context.status(500);
          context.json({
            reason: "Internal Server Error."
          });
        });
    } else {
      context.status(404);
      context.json({
        reason: "User do not exists."
      });
    }
  } else  {
    context.status(400).json({
      error: "Bad Request, missing email"
    });
  }
});