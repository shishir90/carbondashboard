const { authenticate } = require("../lib/auth");
const sendEmail = require('../lib/sendEmail/index');
const inviteHtmlBody = require('../lib/sendEmail/templates/inviteHtml');

const emailSubject = "This is an invite ";

module.exports = authenticate(async function (context, req) {
  if(req.body
    && req.body.type
    && req.body.name
    && req.body.recipient
  ) {
    return sendEmail(
      emailSubject,
      inviteHtmlBody({
        type : req.body.type,
        name : req.body.name
      }),
      req.body.recipient
    )
      .then(res => context.status(200).json({reason: "Message sent"}))
      .catch(err => {
        console.error(err)
        context.status(500).json({reason: "Send message Failed"})
      });
  } else {
    context.status(400).json({
      error: "Bad Request, missing info"
    });
  }
});
