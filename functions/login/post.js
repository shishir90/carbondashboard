const User = require("../lib/mongoose/User");
const AzADJWT = require("../lib/azad/jwtValidator");
const jwt = require("../lib/auth/jwt");
const tsAndCs = require('../lib/auth/tsAndCs');

module.exports = async function (context, req) {
  try {
    let decodedToken = await AzADJWT(req);
    const { forename, surname, email } = {
      forename: decodedToken.given_name,
      surname: decodedToken.family_name,
      email: decodedToken.upn,
    };
    return User.findOne({email})
    .populate('role') // <--
    .exec()
      .then(user => {
        if (user) {
          user.forename = forename;
          user.surname = surname;
          return user.save()
            .then(() => jwt.sign(user.toObject()))
            .then(token => {
              if(tsAndCs(user)) {
                context.json({
                  token
                });
              } else {
                context.status(412)
                  .json({
                    reason: "Unaccepted T's & C's"
                  });
              }
            })
            .catch(e => {
              context.log("Error", e);
              context.status(500);
              context.json({
                reason: "Failed to update."
              });
            });
        } else {
          context.status(403);
          context.json({
            reason: "You do not have permission to access this resource."
          });
        }
      });
  } catch (e) {
    context.log("error", e);
    context.status(401);
    context.json({
      reason: "Unauthorized",
      error: e.name
    });
  }
};