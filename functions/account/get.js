const { authenticate } = require("../lib/auth");
const getAccountPermissions = require('../lib/functions/getAccountRestrictions');
const GetData = require("../lib/database/getData");
const mongo = new GetData(process.env.CARBON_GENERIC_CONNECTION_STRING, process.env.CARBON_DATA_DB_NAME);

module.exports = authenticate(async function (context, req) {
  if(context.bindingData && context.bindingData.id) {
    return getAccountPermissions(context.user)
      .then(restrictions => {
        const query = {
          $and: [ Object.assign(
            {},
            {"_id": context.bindingData.id + ""},
            restrictions ? restrictions : null
          )]};
        return Promise.all(
          [
            mongo.getData("account",query),
            mongo.getData("account_aggregate",query)
          ]
        )
          .then(data => {
            if(data[0].status !== 200) {
              context.status(data[0].status).json(data[0].docs);
            } else {
              context.json(
                Object.assign(
                  {},
                  (data[0].docs && data[0].docs[0] ? data[0].docs[0] : data[0].docs),
                  {aggregations: data[1].docs}
                )
              )
            }
          })
          .catch(err => {
            context.status(500).json({reason: "Internal Server Error : " + err});
          });
      })
      .catch(error => {
        console.log(error);
        context.status(401).json({
          error: error
        })
      });
  } else  {
    context.status(400).json({
      error: "Bad Request, missing id"
    });
  }
});