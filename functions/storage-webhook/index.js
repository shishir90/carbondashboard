const methodDelegator = require("../lib/functions/methodDelegator");

const POST = require("./post");

module.exports = methodDelegator({
  POST
});