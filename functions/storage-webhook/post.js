
const blobCreated = require("./blobCreated");

module.exports = async function (context, request) {
    let validationResponse;
    await Promise.all(request.body.map(async event => {
        switch(event.eventType) {
            case "Microsoft.EventGrid.SubscriptionValidationEvent": {
                validationResponse = event.data.validationCode;
                break;
            }
            case "Microsoft.Storage.BlobCreated": {
                await blobCreated(event);
                break;
            }
        }
    }));
    context.json({
        success: true,
        validationResponse
    });
};