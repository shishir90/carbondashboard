const StorageClient = require("../lib/storage");
const CSVParse = require("../lib/csvparser/");
const DatabaseUpdate = require("../lib/database/streamingUpdate");
const { DataIngested, PreAccountAggregation } = require("../lib/eventgrid");

const STORAGE_CONTAINER = "input-files";

const client = new StorageClient(STORAGE_CONTAINER);

module.exports = async function (event) {
    if (event.subject) {
        let blobName = event.subject.split("/").pop();
        let response = await client.getBlob(blobName);
        return new Promise((resolve, reject) => 
            CSVParse.parseCSVFromStream(response.readableStreamBody)
                .pipe(new CSVParse.streamObjectToModel())
                .pipe(new DatabaseUpdate(null, (queueNames, startDate, endDate) => {
                  resolve(Promise.all(queueNames.map((ingestedType) => {
                    let dataIngestedEvent = new DataIngested(ingestedType, startDate, endDate);
                    if (ingestedType.toLowerCase() === "account") {
                        let preAccountAggregationEvent = new PreAccountAggregation();
                        return Promise.all([
                            preAccountAggregationEvent.fire(),
                            dataIngestedEvent.fire()
                        ]);
                    }
                    return dataIngestedEvent.fire();
                  })));
                })));
    } else {
        reject();
    }
}