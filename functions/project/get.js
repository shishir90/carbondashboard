const { authenticate } = require("../lib/auth");
const getDataById = require("../lib/functions/getDataById");
const getProjectPermissions = require('../lib/functions/getProjectRestrictions');

module.exports = authenticate(async function (context, req) {
  return getDataById(context, req, "project", getProjectPermissions, "project_aggregate", null, true);
});