const flat = require("array-flatten");
const { authenticate } = require("../lib/auth");
const MongoClient = require("../lib/database/mongoClient");
const getProjectPermissions = require('../lib/functions/getProjectRestrictions');

const DB_URL = process.env.CARBON_GENERIC_CONNECTION_STRING;
const DATA_DB = process.env.CARBON_DATA_DB_NAME;

const DATA_TYPES = [
  "hotel",
  "train",
  "car",
  "taxi",
  "flight"
];

module.exports = authenticate(async function (context, req) {
  let permissions = await getProjectPermissions(context.user);
  const mongo = new MongoClient(DB_URL, DATA_DB);
  const { startIndex, count, sortField, search, monthKey, includeCount } = req.query;
  let splitMonth = monthKey.split("-");
  let year = splitMonth[0];
  let month = Number(splitMonth[1]);

  let MONTH_BLOCKS = [];

  for (let i = month; i >= 0; i--) {
    MONTH_BLOCKS.push(year + "-" + i);
  }

  if (context.bindingData && context.bindingData.id) {
    return mongo.getDB()
    .then(db => {
      let searchQuery = {};
      if (search) {
        searchQuery.$or = [{ description: { $regex: "^" + search, $options: 'i' }}, { _id: { $regex: "^" + search, $options: 'i' }}];
      }
      let promises = [
        db.collection("project").aggregate([
          {
            $match: Object.assign({}, searchQuery, {
              projectCustomerNumber: context.bindingData.id + ""
            })
          },
          {
            $match: permissions
          },
          {
            $lookup:
               {
                  from: "project_aggregate",
                  localField: "_id",
                  foreignField: "_id",
                  as: "aggregates"
              }
         },
         {
           $match: {
             aggregates: {
               $exists: true, 
               $ne: [] 
             }
           }
         },
         {
            $addFields: { 
              "aggregate": {
                $arrayElemAt: [ "$aggregates", 0 ] 
              }
            }
         },
         {
            $addFields: { 
              "sortField": sortField,
              "summary.avgNightsAway": {
                $divide: [`$aggregate.hotel.${monthKey}.numberOfNights`, `$aggregate.hotel.${monthKey}.bookingCount`]
              },
              "summary.monthCost": {
                $add: DATA_TYPES.map(type => ({
                  $ifNull: [`$aggregate.${type}.${monthKey}.totalCost`, 0]
                }))
              },
              "summary.annualCost": {
                $add: flat(MONTH_BLOCKS.map(month => DATA_TYPES.map(type => ({
                  $ifNull: [`$aggregate.${type}.${month}.totalCost`, 0]
                }))))
              },
              "summary.carbonPerHead": {
                $add: DATA_TYPES.map(type => ({
                  $divide: [
                    {
                      $ifNull: [`$aggregate.${type}.${monthKey}.totalCarbon`, 0]
                    },
                    {
                      $size: {
                        $ifNull: [`$aggregate.${type}.${monthKey}.employees`, [null]]
                      },
                    }
                  ]
                }))
              },
              "summary.costPerHead": {
                $add: DATA_TYPES.map(type => ({
                  $divide: [
                    {
                      $ifNull: [`$aggregate.${type}.${monthKey}.totalCost`, 0]
                    },
                    {
                      $size: {
                        $ifNull: [`$aggregate.${type}.${monthKey}.employees`, [null]]
                      },
                    }
                  ]
                }))
              }
            }
         },
          {
            $facet: {
              edges: [
                {
                  $sort: {
                    [sortField]: -1
                  }
                },
                { $skip: Number(startIndex || "0") },
                { $limit: Number(count || "15") },
              ],
              pageInfo: [
                { $group: { _id: null, count: { $sum: 1 } } },
              ],
            },
          },
          {
            $unwind: "$edges"
          },
          { $replaceRoot: { newRoot: "$edges" } },
        ]).toArray()
      ];

      if (includeCount) {
        promises.push(db.collection("project").aggregate([
            {
              $match: Object.assign({}, searchQuery, {
                projectCustomerNumber: context.bindingData.id + ""
              })
            },
            {
              $match: permissions
            },
            {
              $lookup:
                 {
                    from: "project_aggregate",
                    localField: "_id",
                    foreignField: "_id",
                    as: "aggregates"
                }
           },
           {
             $match: {
               aggregates: {
                 $exists: true, 
                 $ne: [] 
               }
             }
           },
           {
            $count: "documentCount"
           }
        ]).toArray()
          .then(arr => arr[0]));
      } 
      return Promise.all(promises)
    })
    .then(r => context.json({
      rows: r[0],
      documentCount: r[1] && r[1].documentCount
    }))
    .catch(error => context.status(500).json(error));
  } else {
    // No idea how you got here tbh?
    context.status(400).json({
      error: "Missing account ID"
    })
  }
});