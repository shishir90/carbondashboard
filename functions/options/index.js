const originsHelper = require("../lib/origins");
module.exports = async function (context, req) {
  context.res = {
    status: 200,
    headers: {
      "Allow": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      "Access-Control-Allow-Origin": originsHelper(req.headers.origin),
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type,Authorization'
    },
    body: "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  };
};