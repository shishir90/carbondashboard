const Role = require("../lib/mongoose/Role");
const create = require("../lib/mongoose/actions/create");

module.exports = async function (context, req) {

  // [TODO] Add Authorization

  return create(context, req, Role, "name");
};