const methodDelegator = require("../lib/functions/methodDelegator");

const POST = require("./post");
const GET = require("./get");

module.exports = methodDelegator({
  POST,
  GET
});