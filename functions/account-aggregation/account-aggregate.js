const MongoClient = require("../lib/database/mongoClient");

const DB_URL = process.env.CARBON_GENERIC_CONNECTION_STRING;
const DATA_DB = process.env.CARBON_DATA_DB_NAME;

const DATA_TYPES = [
    "hotel",
    "train",
    "car",
    "taxi",
    "flight"
];

const dataArrayConfig = DATA_TYPES.reduce((out, type) => Object.assign({}, out, {
    [type]: { $push: "$" + type }
}), {});


module.exports = event => {
    const mongo = new MongoClient(DB_URL, DATA_DB);
    return mongo.getDB()
    .then(db => {
        return db.collection("project_aggregate").aggregate([
            {
               $lookup:
                  {
                     from: "project",
                     localField: "_id",
                     foreignField: "_id",
                     as: "metadata"
                 }
            },
            {
                $group: Object.assign({
                    _id: "$metadata.projectCustomerNumber"
                }, dataArrayConfig)
            }
        ], { allowDiskUse: true }).toArray()
        .then(documents => documents.map(document => Object.assign({}, document, {
            _id: document._id[0]
        }, Object.keys(document).filter(k => DATA_TYPES.indexOf(k) >= 0).reduce((out, type) => Object.assign({}, out, {
            [type]: document[type].reduce((out, typeGroup) => Object.assign({}, out, Object.entries(typeGroup).reduce((out, [month, values]) => Object.assign({}, out, {
                [month]: Object.assign({}, out[month], Object.entries(values).reduce((res, [key, value]) => Object.assign({}, res, {
                    [key]: typeof value === 'number' ? value + ((out[month] && out[month][key]) || 0) : [].concat((out[month] && out[month][key]) || [], value)
                }), {}))
            }), out)), {})
        }), {
        }))))
        .then(result => {
            let bulk = db.collection("account_aggregate").initializeOrderedBulkOp();
            result.forEach(document => bulk.find( { _id: document._id } ).upsert().updateOne({
                $set: Object.entries(document)
                    .reduce((setValue, entry) => Object.assign({}, setValue, {
                        [entry[0]]: entry[1]
                    }), { lastModified: new Date(Date.now()) })
            }));
            return bulk.execute();
        });
    });
};