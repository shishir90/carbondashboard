const handler = require("./ingestion-handlers");

module.exports = async function (context, request) {
    let validationResponse;
    await Promise.all(request.body.map(async event => {
        switch(event.eventType) {
            case "Microsoft.EventGrid.SubscriptionValidationEvent": {
                validationResponse = event.data.validationCode;
                break;
            }
            case "DataIngested": {
                // await blobCreated(event);
                await handler(event);
                break;
            }
        }
    }));
    context.json({
        success: true,
        validationResponse
    });
};