
const Aggregation = require("../../lib/aggregation/Aggregation");

module.exports = (timeBand) => [
    new Aggregation("totalDistance", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                totalDistance: {
                    $sum: "$distance"
                } 
            }
        }      
    ]),
    new Aggregation("numberOfTrips", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfTrips: {
                    $sum: 1
                } 
            }
        }
    ])
];