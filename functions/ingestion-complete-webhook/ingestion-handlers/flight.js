
const Aggregation = require("../../lib/aggregation/Aggregation");

module.exports = (timeBand) => [
    new Aggregation("totalDistance", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                totalDistance: {
                    $sum: "$flightDistance"
                } 
            }
        }      
    ]),
    new Aggregation("numberOfTrips", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfTrips: {
                    $sum: 1
                } 
            }
        }
    ]),
    new Aggregation("routes", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "origin": "$originCity",
                    "destination": "$destinationCity"
                },
                routeUsedCount: {
                    $sum: 1
                } 
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                routes: { 
                    "$push": { 
                        "origin": "$_id.origin",
                        "destination": "$_id.destination",
                        "count": "$routeUsedCount"
                    },
                }
            }
        },    
    ])
];