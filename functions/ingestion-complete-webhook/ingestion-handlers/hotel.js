
const Aggregation = require("../../lib/aggregation/Aggregation");

module.exports = (timeBand) => [
    new Aggregation("numberOfNights", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfNights: {
                    $sum: "$nights"
                } 
            }
        }      
    ]),
    new Aggregation("bookingCount", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                bookingCount: {
                    $sum: 1
                } 
            }
        }
    ]),
    new Aggregation("hotelNames", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "hotelName": "$hotelDescription"
                },
                hotelNameCount: {
                    $sum: 1
                } 
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                hotelNames: { 
                    "$push": { 
                        "hotelName": "$_id.hotelName",
                        "count": "$hotelNameCount"
                    },
                }
            }
        },    
    ])
];