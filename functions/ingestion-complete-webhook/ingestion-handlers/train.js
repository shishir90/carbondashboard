
const Aggregation = require("../../lib/aggregation/Aggregation");

module.exports = (timeBand) => [
    new Aggregation("totalDistance", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                totalDistance: {
                    $sum: "$distance"
                } 
            }
        }      
    ]),
    new Aggregation("numberOfTrips", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfTrips: {
                    $sum: 1
                } 
            }
        }
    ]),
    new Aggregation("numberOfReturns", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $match: {
                "isReturn": true
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfReturns: {
                    $sum: 1
                } 
            }
        }
    ]),
    new Aggregation("numberOfSingles", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $match: {
                "isReturn": false
            }
        },
        {
            $group: {
                _id: "$projectCode",
                numberOfSingles: {
                    $sum: 1
                } 
            }
        }
    ]),
    new Aggregation("routes", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "origin": "$origin",
                    "destination": "$destination"
                },
                routeUsedCount: {
                    $sum: 1
                } 
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                routes: { 
                    "$push": { 
                        "origin": "$_id.origin",
                        "destination": "$_id.destination",
                        "count": "$routeUsedCount"
                    },
                }
            }
        },    
    ])
];