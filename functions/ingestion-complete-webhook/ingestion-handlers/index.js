const MongoClient = require("../../lib/database/mongoClient");
const Aggregation = require("../../lib/aggregation/Aggregation");
const { PreAccountAggregation } = require("../../lib/eventgrid");

const hotelAggregates = require("./hotel");
const trainAggregates = require("./train");
const carAggregates = require("./car");
const taxiAggregates = require("./taxi");
const flightAggregates = require("./flight");

const DB_URL = process.env.CARBON_GENERIC_CONNECTION_STRING;
const DATA_DB = process.env.CARBON_DATA_DB_NAME;

const getTimeBands = (startDate, endDate) => {
    const lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 1);

    let timeBands = [];
    let currentTime = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
    while (currentTime < lastDay) {
        let lastDayInMonth = new Date(currentTime.getFullYear(), currentTime.getMonth() + 1, 1);
        timeBands.push({
            startBand: new Date(currentTime),
            endBand: lastDayInMonth,
            label: `${currentTime.getFullYear()}-${currentTime.getMonth()}`
        });
        currentTime.setMonth(currentTime.getMonth() + 1);
    }
    return timeBands;
}

const AGGREGATES = {
    "hotel": hotelAggregates,
    "train": trainAggregates,
    "car": carAggregates,
    "taxi": taxiAggregates,
    "flight": flightAggregates
};

const SHARED_AGGREGATES = timeBand => [
    new Aggregation("totalCost", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                totalCost: {
                    $sum: "$cost"
                } 
            }
        }      
    ]),
    new Aggregation("totalCarbon", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: "$projectCode",
                totalCarbon: {
                    $sum: "$emissionsKG"
                } 
            }
        }      
    ]),
    new Aggregation("employees", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "employeeId": "$employeeId"
                },
                employeeCount: {
                    $sum: 1
                },
                employeeCost: {
                    $sum: "$cost"
                },
                employeeCarbon: {
                    $sum: "$emissionsKG"
                },
                employeeDistance: {
                    $sum: "$distance"
                },
                avgEmployeeCost: {
                    $avg: "$cost"
                },
                avgEmployeeCarbon: {
                    $avg: "$emissionsKG"
                },
                avgEmployeeDistance: {
                    $avg: "$distance"
                }
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                employees: { 
                    "$push": { 
                        "employeeId": "$_id.employeeId",
                        "count": "$employeeCount",
                        "totalCost": "$employeeCost",
                        "totalCarbon": "$employeeCarbon",
                        "totalDistance": "$employeeDistance",
                        "avgCost": "$avgEmployeeCost",
                        "avgCarbon": "$avgEmployeeCarbon",
                        "avgDistance": "$avgEmployeeDistance"
                    },
                },
                totalEmployeeCount: {
                    "$sum": 1
                }
            }
        },    
    ]),
    new Aggregation("reasonsForTravel", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "travelReason": "$travelReason"
                },
                travelReasonCount: {
                    $sum: 1
                } 
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                reasonsForTravel: { 
                    "$push": { 
                        "travelReason": "$_id.travelReason",
                        "count": "$travelReasonCount"
                    },
                }
            }
        },    
    ]),
    new Aggregation("destinations", [
        {
            $match: {
                "departureDate": {
                    $gte: timeBand.startBand,
                    $lt: timeBand.endBand
                }
            }
        },
        {
            $group: {
                _id: {
                    "projectCode": "$projectCode",
                    "incurredLocation": "$incurredLocation"
                },
                incurredLocationCount: {
                    $sum: 1
                } 
            }
        },
        { 
            $group: {
                _id: "$_id.projectCode",
                destinations: { 
                    "$push": { 
                        "incurredLocation": "$_id.incurredLocation",
                        "count": "$incurredLocationCount"
                    },
                }
            }
        },    
    ])
];

module.exports = event => {
    if (Object.keys(AGGREGATES).indexOf(event.subject.toLowerCase()) < 0) {
        return;
    }
    const mongo = new MongoClient(DB_URL, DATA_DB);
    const startDate = new Date(event.data.startDate);
    const endDate = new Date(event.data.endDate);
    const timeBands = getTimeBands(startDate, endDate);
    return mongo.getDB()
    .then(db => 
        Promise.all(timeBands.map(timeBand => {
            let collection = db.collection(event.subject);
            const aggregations = [].concat(AGGREGATES[event.subject && event.subject.toLowerCase()](timeBand), SHARED_AGGREGATES(timeBand));
            return Promise.all(aggregations.map(aggregation => 
                collection.aggregate(aggregation.aggregationStages, { allowDiskUse: true })
                .toArray()
                .then(arr => arr.map(doc => Object.assign({}, doc, {
                    attribute: aggregation.fieldName
                })))
            ))
            .then(aggregations => ({
                label: timeBand.label,
                aggregations
            }));
        }))
        .then(results => results.reduce((result, timeBand) => Object.assign({}, result || {}, 
            timeBand.aggregations.reduce((aggregationsResult, aggregation) => Object.assign({}, aggregationsResult, 
                aggregation.reduce((reducedResult, aggregationResultDocument) => Object.assign({}, reducedResult, {
                    [aggregationResultDocument._id]: Object.assign({}, result[aggregationResultDocument._id] || {}, aggregationsResult[aggregationResultDocument._id] || {}, reducedResult[aggregationResultDocument._id] || {}, {
                        [timeBand.label]: Object.assign({}, (result[aggregationResultDocument._id] || {})[timeBand.label] || {}, (aggregationsResult[aggregationResultDocument._id] || {})[timeBand.label] || {}, (reducedResult[aggregationResultDocument._id] || {})[timeBand.label] || {}, {
                            [aggregationResultDocument.attribute]: aggregationResultDocument[aggregationResultDocument.attribute]
                        })
                    })
                }), {})), {})), {}))
        .then(result => {
            let bulk = db.collection("project_aggregate").initializeOrderedBulkOp();
            let ip = Object.entries(result);
            console.log("Reaggregated " + ip.length + " documents");
            ip.forEach(document => bulk.find( { _id: document[0] } ).upsert().updateOne({
                $set: Object.entries(document[1])
                    .reduce((setValue, entry) => Object.assign({}, setValue, {
                        [`${event.subject && event.subject.toLowerCase()}.${entry[0]}`]: entry[1]
                    }), { lastModified: new Date(Date.now()) })
            }));
            return bulk.execute();
        }))
        .then(() => {
            // Complete! Aggregate into accounts
            let preAccountAggregationEvent = new PreAccountAggregation();
            return preAccountAggregationEvent.fire();

        });
}