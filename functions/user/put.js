const User = require("../lib/mongoose/User");

module.exports = async function (context, req) {

  // [TODO] Add Authorization

  if (req.body) {
    const { email, role, accounts, projects, forename, surname, id } = req.body;
    let existingUser;
    if(email) {
      existingUser = await User.findOne({email}).exec();
    } else if(id) {
      existingUser  = await User.findById(id).exec();
    }
    if (existingUser) {
      if(forename) existingUser.forename = forename;
      if(surname) existingUser.surname = surname;
      if(accounts instanceof Array && accounts.length > 0 && accounts[0]) existingUser.accounts = accounts;
      if(projects instanceof Array && projects.length > 0 && projects[0]) existingUser.projects = projects;
      if(role) existingUser.role = role;
      return existingUser.save()
        .then(() => {
          context.json({
            success: true,
            document: existingUser
          });
        })
        .catch(e => {
          context.error(e);
          context.status(400);
          context.json({
            reason: "Invalid Request"
          });
        });
    } else {
      context.status(404);
      context.json({
        reason: "User do not exists."
      });
    }
  }
  else {
    context.status(400);
    context.json({
      reason: "Invalid Request Body"
    });
  }
};