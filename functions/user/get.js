const User = require("../lib/mongoose/User");
const { authenticate } = require("../lib/auth");
const getData = require("../lib/mongoose/actions/getData");

module.exports = authenticate(async function (context, req) {

  if(!context.user) {
    context.status(401).json({
      error: "Unauthorized User"
    });
  } else if(!user.role || !user.role.permissions.users.read) {
    context.status(403).json({
      error: "Insufficient Permissions"
    });
  }

  return getData(context,req, User, "email");

});

