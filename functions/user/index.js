const methodDelegator = require("../lib/functions/methodDelegator");

const GET = require("./get");
const POST = require("./post");
const PUT = require("./put");

module.exports = methodDelegator({
  GET,
  POST,
  PUT
});