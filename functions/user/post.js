const User = require("../lib/mongoose/User");
const create = require("../lib/mongoose/actions/create");

module.exports = async function (context, req) {

  // [TODO] Add Authorization

  return create(context, req, User, "email");

};