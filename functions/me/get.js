const { authenticate } = require("../lib/auth");
const MongoClient = require("../lib/database/mongoClient");
const mongo = new MongoClient(process.env.CARBON_GENERIC_CONNECTION_STRING, process.env.CARBON_DATA_DB_NAME);

const getAccounts = (db, query, reqQuery) => {
  const { startIndex, count, sortField, search, includeCount } = reqQuery;
  let searchQuery = {};
  if (search) {
    searchQuery.$or = [{ name: { $regex: search, $options: 'i' }}, { _id: { $regex: "^" + search, $options: 'i' }}];
  }

  return db.collection("account_aggregate").aggregate([
    {
      $project: {
        _id: "$_id"
      }
    }
  ]).toArray()
    .then(ids => ids.map(doc => doc._id))
    .then(idArray => {
      let promises = [
        db.collection("account").aggregate([
          {
            $match: Object.assign({}, query || {}, searchQuery || {})
          },
          {
            $match: {
              _id: {
                $in: idArray
              }
            }
          },
          {
            $facet: {
              edges: [
                {
                  $sort: {
                    [sortField]: 1
                  }
                },
                { $skip: Number(startIndex || "0") },
                { $limit: Number(count || "15") },
              ],
              pageInfo: [
                { $group: { _id: null, count: { $sum: 1 } } },
              ],
            },
          },
          {
            $unwind: "$edges"
          },
          { $replaceRoot: { newRoot: "$edges" } }
        ], { allowDiskUse: true }).toArray()
      ];
      if (includeCount) {
        promises.push(
          db.collection("account").aggregate([
            {
              $match: Object.assign({}, query || {}, searchQuery || {})
            },
            {
              $match: {
                _id: {
                  $in: idArray
                }
              }
            },
            {
              $count: "docCount"
            }
          ])
          .toArray()
          .then(arr => arr && arr[0] && arr[0].docCount)
          
        );
      }
      return Promise.all(promises);
    })

};

const getProjectsAndAccountsForUser = (user, query) => {
    let accounts = user.accounts;
    let permissions = user.role.permissions;
    if (permissions && permissions.accounts && permissions.accounts.all) {
        return mongo.getDB()
            .then(db => Promise.all([
                getAccounts(db, null, query),
            ]));
    } else if (permissions && permissions.projects && permissions.projects.all) {
        return mongo.getDB()
            .then(db => Promise.all([
                getAccounts(db, { _id: { $in: accounts }}, query),
            ]));
    } else {
        return mongo.getDB()
            .then(db => Promise.all([
                getAccounts(db, { _id: { $in: accounts }}, query),
            ]));
    }
};

const getProjectsForUser = (user) => {
  return mongo.getDB()
    .then(db => {
      return db.collection("project").find({"_id": {$in: user.projects}}).toArray()
    });
};

module.exports = authenticate(async function (context, req) {
    let { user } = context;
    
    if (user) {
        return getProjectsAndAccountsForUser(user, req.query)
        .then(([accounts]) => {
          return getProjectsForUser(user)
            .then(projects => {
              context.json({
                accounts: {
                  rows: accounts[0],
                  documentCount: accounts[1]
                },
                projects:{
                  rows: projects
                }
              })
            })
        })
        .catch(e => {
          context.status(500).json(e)
        })
    } else {
        // error
        return Promise.resolve();
    }
});