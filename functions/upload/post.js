const uuid = require("uuid");

const { authenticate } = require("../lib/auth");
const User = require("../lib/mongoose/User");
const StorageClient = require("../lib/storage/oldClient");

const STORAGE_CONTAINER = "input-files";

const client = new StorageClient(STORAGE_CONTAINER);

module.exports = authenticate(async function (context, request) {
  if (!context.user) {
    context.status(401);
    context.json({
      error: "Unauthorized"
    }); 
  } else if (!context.user.role || !context.user.role.permissions.data.upload) {
    context.status(403);
    context.json({
      error: "Insufficient Permissions"
    }); 
  } else {
    try {
      const filename = request.body.filename + uuid.v1() + ".csv";

      let SAS = await client.generateSasToken(filename, "w");
      context.json({
        SAS,
        filename
      }); 
    } catch (e) {
      context.status(500);
      context.json({
        error: e.message
      }); 
    }
  }
});
