# K8s Mongo Setup

1) Setup a kubernetes cluster, we're using AKS (Azure Kubernetes Service)

2) Get a kubectl connection to the cluster by following the instructions for AKS (or general kubernetes instructions)

3) To deploy all the things, run the following:
```
kubectl apply -f azure-hdd.yaml
kubectl apply -f mongo-db.yaml
kubectl apply -f mongo-load.yaml
```

4) After 5/10 mins or so, everything should provision. The mongo-0 service should have a publically accessible IP.