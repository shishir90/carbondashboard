const express = require("express");
const app = express();
const AD_MOCK = require("./ad-mock.json");


app.get("/.auth/me", (req, res) => {
  res.json(AD_MOCK);
});

app.get("/.auth/login/aad", (req, res) => {
  res.redirect("/");
})

app.listen(1756);