## Quick Links

https://carbon-dashboard-preprod.azurewebsites.net/ </br>
https://carbon-dashboard.azurewebsites.net/

## Getting it running (just the UI, quick method)

1) Install dependencies in /ui 
```
yarn
```
2) Create a .env in /ui with the following:
```
NODE_PATH=src/
REACT_APP_BYPASS_AUTH=true
```
3) `yarn start`


## Getting it running (with Auth)

1) Install dependencies in /ui, /mocks/ad and /functions 
```
yarn
```
2) Create a .env in /ui with the following:
```
NODE_PATH=src/
```
2) Create a .env in /functions with the following:
```
CARBON_DATA_DB_CONNECTION_STRING=
CARBON_META_DB_CONNECTION_STRING=
JWT_SECRET=any-random-string
```
3) Copy/paste the contents of /functions/dotenv.sh into a bash window
4) Open the production or staging site and login
5) Open Chrome Devtools console and execute:
```
fetch("/.auth/me").then(r => r.json()).then(JSON.stringify).then(console.log)
```
6) Paste the json logged out into /mocks/ad/ad-mock.json
7) Run `yarn start` in all 3 directories

## Using a mock user

1) add `MOCK_USER=true` to /functions/.env
2) create /functions/mockuser.json file with similar to 
```
{
  "_id": "userid",
  "accounts": [],
  "projects": [],
  "forename": "Mock",
  "surname": "User",
  "email": "mock.user@capgemini.com",
  "role": {
    "_id": "roleid",
    "permissions": {
      "accounts": {
        "all": false,
        "invite": false
      },
      "projects": {
        "all": false,
        "invite": false
      },
      "data": {
        "upload": false
      }
    },
    "name": "mockrole"
  }
}
```