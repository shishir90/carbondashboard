import jwtDecode from 'jwt-decode';
export default new class TokenStore {
  constructor() {
    this._token = null;
  }
  get decoded() {
    return this.token && jwtDecode(this.token);
  }
  get token() {
    return this._token;
  }
  set token(token) {
    this._token = token;
  }
  isAuthenticated() {
    return !!this._token;
  }
}();