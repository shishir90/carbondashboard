const colors = {
    brandColor: '#2b0a3d',
    baseColor: '#354052',
    baseColorMedium: '#8a96a0',
    baseColorLight: '#b4bac6',
    baseColorLightest: '#aab2c0',
    background: '#EDEDED',
    background2: '#F1F1F1',
    accent1: '#FF304C',
    accent2: '#12ABDB',
    accent3: '#95E616',
    accent4: '#f5a623',
    error: '#d0021b',
    success: '#7ed321',
    borderColor1: '#EBEDF8',
}

export {
    colors
}
