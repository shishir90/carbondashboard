import AzureStorage from 'azure-storage/browser/azure-storage.blob.export';
import TokenStore from 'authentication/tokenStore';

export default new class {
  uploadFile(file) {
    // Fire off fetch
    return fetch(`${process.env.REACT_APP_FUNCTIONS_EP || ""}/api/upload`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${TokenStore.token}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        filename: file.name
      })
    })
      .then(r => r.json())
      .then(j => {
        if (j.SAS && j.SAS.token && j.SAS.uri && j.filename) {
          return this._uploadFile(file, j);
        } else {
          throw new Error("Failed to authenticate/retrieve SAS");
        }
      })
  }
  _uploadFile(file, response) {
    return new Promise((resolve, reject) => {
      let blobService = AzureStorage.createBlobServiceWithSas("https://carbondashboard.blob.core.windows.net", response.SAS.token).withFilter(new AzureStorage.ExponentialRetryPolicyFilter());
      blobService.createBlockBlobFromBrowserFile('input-files', response.filename, file, {}, (error, result, response) => {
          if (error) {
              // Upload file failed
              reject(error)
          } else {
              // Upload successfully
              resolve();
          }
      });
    })
  }
}();