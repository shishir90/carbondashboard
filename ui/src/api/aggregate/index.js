import TokenStore from 'authentication/tokenStore';

export default new class {
    getAggregatesByProject(projectId) {
        // Fire off fetch
        return fetch(`${process.env.REACT_APP_FUNCTIONS_EP || ""}/api/aggregate/project/${projectId}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${TokenStore.token}`
        }
        })
        .then(r => r.json());
    }
}();