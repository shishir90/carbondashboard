import TokenStore from 'authentication/tokenStore';

export default new class {
    getMe(startIndex, count, sortField, search, includeCount) {
        let query = {
          startIndex,
          count,
          sortField,
          search,
          includeCount
        };
        const queryString = Object.entries(query).filter(entry => entry[0] && entry[1]).map(entry => entry.join("=")).join("&");
        // Fire off fetch
        return fetch(`${process.env.REACT_APP_FUNCTIONS_EP || ""}/api/me?${queryString}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${TokenStore.token}`
        }
        })
        .then(r => {
            switch(r.status) {
                case 200: {
                    return r;
                }
                default: {
                    throw new Error("Bad status code");
                }
            }
        })
        .then(r => r.json());
    }
}();