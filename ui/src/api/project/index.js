import TokenStore from 'authentication/tokenStore';

export default new class {
    getProject(projectId) {
        // Fire off fetch
        return fetch(`${process.env.REACT_APP_FUNCTIONS_EP || ""}/api/project/${projectId}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${TokenStore.token}`
        }
        })
        .then(r => {
            switch(r.status) {
                case 200: {
                    return r;
                }
                default: {
                    throw new Error("Bad status code");
                }
            }
        })
        .then(r => r.json());
    }
}();