import flat from "array.prototype.flat";

class Statistic {
  constructor(description, value) {
    this.description = description;
    this.value = value;
  }
}
const MONTHS = [
  "JANUARY",
  "FEBRUARY",
  "MARCH",
  "APRIL",
  "MAY",
  "JUNE",
  "JULY",
  "AUGUST",
  "SEPTEMBER",
  "OCTOBER",
  "NOVEMBER",
  "DECEMBER"
];

let summariseYear = (blocks, month) => ({
  total: blocks
    .filter(block => Number(block.key.split("-")[1]) <= month)
    .reduce(
      (blockResult, block) =>
        Object.assign(
          {},
          blockResult,
          Object.keys(block).reduce(
            (result, key) =>
              Object.assign({}, result, {
                [key]: Array.isArray(block[key])
                  ? [].concat(blockResult[key] || [], block[key] || [])
                  : block[key] + (blockResult[key] || 0)
              }),
            {}
          )
        ),
      {}
    ),
  count: blocks
    .filter(block => Number(block.key.split("-")[1]) <= month)
    .reduce(
      (blockResult, block) =>
        Object.assign(
          {},
          blockResult,
          Object.keys(block).reduce(
            (result, key) =>
              Object.assign({}, result, {
                [key]: 1 + (blockResult[key] || 0)
              }),
            {}
          )
        ),
      {}
    )
});

const nycFlightEstimate = tonnes => {
  let numberOfFlights = tonnes / 0.96;
  let rounded = Math.round(numberOfFlights);
  if (rounded > numberOfFlights) {
    return `almost ${rounded} return flights to New York City`;
  } else if (rounded === 0) {
    return `almost a return flight to New York City`;
  } else {
    return `more than ${rounded} return flights to New York City`;
  }
};

function numberWithCommas(x) {
  if (x.toString().indexOf(".") >= 0) {
    let n = Number(x).toFixed(2);
    let split = n.toString().split(".");
    return [numberWithCommas(split[0]), split[1]].join(".");
  }
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const getActiveBlocks = (aggregates, year) => {
  let blocks = Object.keys(aggregates || {})
    .filter(key => key.includes(year + "-"))
    .map(k =>
      Object.assign({}, aggregates[k], {
        key: k
      })
    )
    .sort((a, b) => {
      let splitA = a.key.split("-").map(Number);
      let splitB = b.key.split("-").map(Number);
      return splitA[1] - splitB[1];
    });
  if (blocks.length === 0) {
    return [];
  }
  let last = blocks[blocks.length - 1];
  let splitLastKey = last.key.split("-").map(n => Number(n));
  let lastKeyMonth = splitLastKey[1];
  let lastKeyYear = splitLastKey[0];

  let arr = [];

  let keys = blocks.reduce(
    (result, block) =>
      Object.assign({}, result, {
        [block.key]: block
      }),
    {}
  );
  for (let x = 0; x <= lastKeyMonth; x++) {
    let k = `${lastKeyYear}-${x}`;
    if (keys[k]) {
      arr.push(
        Object.entries(last).reduce(
          (result, entry) =>
            Object.assign(
              {},
              result,
              {
                [entry[0]]: entry[1] && (Array.isArray(entry[1]) ? [] : 0)
              },
              { key: k },
              keys[k]
            ),
          {}
        )
      );
    } else {
      arr.push(
        Object.entries(last).reduce(
          (result, entry) =>
            Object.assign(
              {},
              result,
              {
                [entry[0]]: entry[1] && (Array.isArray(entry[1]) ? [] : 0)
              },
              { key: k }
            ),
          {}
        )
      );
    }
  }

  return arr;
};

const aggregateAcrossTypes = aggregates =>
  aggregates.reduce(
    (output, aggregate) =>
      Object.assign(
        {},
        output,
        Object.keys(aggregate || {}).reduce(
          (out, key) =>
            Object.assign({}, output, out, {
              [key]: Array.isArray(aggregate[key])
                ? [].concat(output[key] || [], aggregate[key] || [])
                : (output[key] || 0) + (aggregate[key] || 0)
            }),
          {}
        ),
        {}
      ),
    {}
  );

const getAnnualSummary = aggregates => ({
  count: aggregateAcrossTypes(
    aggregates.map((aggregate => aggregate && aggregate.count) || {})
  ),
  total: aggregateAcrossTypes(
    aggregates.map((aggregate => aggregate && aggregate.total) || {})
  )
});

const formatKGToTonnes = n => n && n / 1000;
const formatTo3DP = n => n && Math.round(n * 100) / 100;
const formatTime = h => {
  let hrs = formatTo3DP(h);
  if (!hrs) {
    return hrs;
  } else {
    return `${Math.round(hrs)} hours, ${Math.round(
      (hrs - Math.floor(hrs)) * 60
    )} minutes`;
  }
};

const summariseBlocks = types =>
  Object.entries(
    flat(types).reduce(
      (out, item) =>
        Object.keys(item).reduce(
          (outputTwo, key) =>
            Object.assign({}, out, outputTwo, {
              [item.key]: Object.assign(
                {},
                out[item.key],
                outputTwo[item.key],
                {
                  [key]: Array.isArray(item[key])
                    ? [].concat(
                        (outputTwo[item.key] || out[item.key] || {})[key] || [],
                        item[key]
                      )
                    : ((outputTwo[item.key] || out[item.key] || {})[key] || 0) +
                      item[key]
                }
              )
            }),
          out
        ),
      {}
    )
  )
    .map(entry =>
      Object.assign({}, entry[1], {
        key: entry[0]
      })
    )
    .sort((a, b) => Number(a.key.split("-")[1]) - Number(b.key.split("-")[1]));

const getUniqueTravellers = arr =>
  arr &&
  arr.reduce(
    (result, employee) =>
      Object.assign({}, result, {
        [employee.employeeId]: Object.assign(
          {},
          result[employee.employeeId],
          Object.keys(employee).reduce(
            (res, key) =>
              Object.assign({}, res, {
                [key]: Array.isArray(employee[key])
                  ? [].concat(res[key] || result[key] || [], employee[key])
                  : (res[key] || result[key] || 0) + employee[key]
              }),
            {}
          )
        )
      }),
    {}
  );

const countTravellers = arr =>
  arr &&
  Object.keys(
    arr.reduce(
      (result, employee) =>
        Object.assign({}, result, {
          [employee.employeeId]: true
        }),
      {}
    )
  ).filter(k => k.length > 0).length;

const getChange = (current, before) =>
  before
    ? formatTo3DP(Math.round((100 * (current - (before || 0))) / (before || 1)))
    : 100;

const aggregatesToData = (aggregates, selectedMonth) => {
  if (!aggregates) {
    return;
  }
  const splitLatestAggregation = Object.keys(
    Object.assign(
      {},
      aggregates.hotel,
      aggregates.car,
      aggregates.train,
      aggregates.taxi,
      aggregates.flight
    )
  )
    .map(i => i.split("-").map(Number))
    .sort((a, b) => (a[0] === b[0] ? a[1] - b[1] : a[0] - b[0]))
    .reverse()[0];
  const activeYear = splitLatestAggregation[0];
  const latestMonth = selectedMonth || selectedMonth === 0 ? selectedMonth : splitLatestAggregation[1];

  const availableMonths = Object.keys(
    Object.assign(
      {},
      aggregates.hotel,
      aggregates.car,
      aggregates.train,
      aggregates.taxi,
      aggregates.flight
    )
  )
    .filter(key => key.includes(activeYear + "-"))
    .map(month => month.split("-")[1])
    .map(month => Number(month))
    .sort((a, b) => a - b);

  const hotelBlocks = getActiveBlocks(aggregates.hotel, activeYear);
  const hotelBlocksLastYear = getActiveBlocks(aggregates.hotel, activeYear - 1);

  const trainBlocks = getActiveBlocks(aggregates.train, activeYear);
  const trainBlocksLastYear = getActiveBlocks(aggregates.train, activeYear - 1);

  const carBlocks = getActiveBlocks(aggregates.car, activeYear);
  const carBlocksLastYear = getActiveBlocks(aggregates.car, activeYear - 1);

  const taxiBlocks = getActiveBlocks(aggregates.taxi, activeYear);
  const taxiBlocksLastYear = getActiveBlocks(aggregates.taxi, activeYear - 1);

  const flightBlocks = getActiveBlocks(aggregates.flight, activeYear);
  const flightBlocksLastYear = getActiveBlocks(
    aggregates.flight,
    activeYear - 1
  );

  const hotelSummary = summariseYear(hotelBlocks, latestMonth);
  const hotelSummaryLastYear = summariseYear(hotelBlocksLastYear, latestMonth);

  const trainSummary = summariseYear(trainBlocks, latestMonth);
  const trainSummaryLastYear = summariseYear(trainBlocksLastYear, latestMonth);

  const carSummary = summariseYear(carBlocks, latestMonth);
  const carSummaryLastYear = summariseYear(carBlocksLastYear, latestMonth);

  const taxiSummary = summariseYear(taxiBlocks, latestMonth);
  const taxiSummaryLastYear = summariseYear(taxiBlocksLastYear, latestMonth);

  const flightSummary = summariseYear(flightBlocks, latestMonth);
  const flightSummaryLastYear = summariseYear(flightBlocksLastYear, latestMonth);

  const annualBlocks = summariseBlocks([
    hotelBlocks,
    trainBlocks,
    carBlocks,
    taxiBlocks,
    flightBlocks
  ]);
  const annualBlocksLastYear = summariseBlocks([
    hotelBlocksLastYear,
    hotelBlocksLastYear,
    carBlocksLastYear,
    taxiBlocksLastYear,
    flightBlocksLastYear
  ]);

  const reversedBlocks = {
    hotel: [...hotelBlocks]
      .reverse()
      .filter(block => Number(block.key.split("-")[1]) <= latestMonth),
    train: [...trainBlocks]
      .reverse()
      .filter(block => Number(block.key.split("-")[1]) <= latestMonth),
    car: [...carBlocks]
      .reverse()
      .filter(block => Number(block.key.split("-")[1]) <= latestMonth),
    taxi: [...taxiBlocks]
      .reverse()
      .filter(block => Number(block.key.split("-")[1]) <= latestMonth),
    flight: [...flightBlocks]
      .reverse()
      .filter(block => Number(block.key.split("-")[1]) <= latestMonth)
  };

  const annualSummary = getAnnualSummary([
    trainSummary,
    hotelSummary,
    carSummary,
    taxiSummary,
    flightSummary
  ]);

  const annualSummaryLastYear = getAnnualSummary([
    trainSummaryLastYear,
    hotelSummaryLastYear,
    carSummaryLastYear,
    taxiSummaryLastYear,
    flightSummaryLastYear
  ]);

  const isRightBlock = block => Number(block && block.key && block.key.split("-")[1]) === latestMonth ? block : false;

  const latestMonthData = Object.assign(
    {},
    getAnnualSummary(
      [
        reversedBlocks.hotel[0],
        reversedBlocks.train[0],
        reversedBlocks.car[0],
        reversedBlocks.taxi[0],
        reversedBlocks.flight[0]
      ].filter(isRightBlock).map(d => ({ count: {}, total: d }))
    ),
    Object.entries({
      hotel: isRightBlock(reversedBlocks.hotel[0]) || {},
      train: isRightBlock(reversedBlocks.train[0]) || {},
      car: isRightBlock(reversedBlocks.car[0]) || {},
      taxi: isRightBlock(reversedBlocks.taxi[0]) || {},
      flight: isRightBlock(reversedBlocks.flight[0]) || {}
    })
      .map(entry => [entry[0], Object.assign({}, entry[1], {
        // uoooooo
        averageDistance: entry[1] && entry[1].totalDistance && entry[1].totalDistance / entry[1].numberOfTrips
      })])
      .reduce((out, entry) => Object.assign({}, out, {
        [entry[0]]: entry[1]
      }), {})
  );
  const latestMonthDataLastYear = Object.assign(
    {},
    getAnnualSummary(
      [].concat(hotelBlocksLastYear, taxiBlocksLastYear, trainBlocksLastYear, carBlocksLastYear, flightBlocksLastYear).filter(isRightBlock).map(d => ({ count: {}, total: d }))
    ),
    {
    }
  );
  const isRightBlockLastMonth = block => Number(block && block.key && block.key.split("-")[1]) === latestMonth - 1 ? block : false;

  const priorMonthData = Object.assign(
    {},
    getAnnualSummary(
      [
        reversedBlocks.hotel[1],
        reversedBlocks.train[1],
        reversedBlocks.car[1],
        reversedBlocks.taxi[1],
        reversedBlocks.flight[1]
      ].filter(isRightBlockLastMonth).map(d => ({ count: {}, total: d }))
    ),
    {
      hotel: isRightBlockLastMonth(reversedBlocks.hotel[1]) || {},
      train: isRightBlockLastMonth(reversedBlocks.train[1]) || {},
      car: isRightBlockLastMonth(reversedBlocks.car[1]) || {},
      taxi: isRightBlockLastMonth(reversedBlocks.taxi[1]) || {},
      flight: isRightBlockLastMonth(reversedBlocks.flight[1]) || {}
    }
  );

  const uniqueTravellers = getUniqueTravellers(
    [].concat(
      latestMonthData.hotel.employees || [],
      latestMonthData.train.employees || [],
      latestMonthData.car.employees || [],
      latestMonthData.taxi.employees || [],
      latestMonthData.flight.employees || []
    )
  );

  const getTotalTravellers = uniqueTravellers => Object.keys(uniqueTravellers).filter(
    k => k.length > 0
  ).length;

  const totalTravellers = getTotalTravellers(uniqueTravellers);

  return {
    activeYear,
    latestMonth,
    availableMonths,
    overview: [
      {
        title: "Carbon",
        xLabel: "Time (Month)",
        yLabel: "Carbon (Tonnes)",
        tooltipFormat: params =>
          `${Number(params.value).toFixed(2)} tonnes for ${params.name} ${params.seriesName}`,
        chart: {
          xAxis: {
            axisPointer: {
              value: MONTHS[latestMonth].substring(0, 3)
            }
          },
          series: [
            {
              name: activeYear,
              type: "line",
              data: [0].concat(
                annualBlocks
                  .reduce(
                    (result, month) => {
                      let last = result.arr[result.arr.length - 1] || 0;
                      return {
                        arr: [].concat(result.arr, [last + month.totalCarbon])
                      };
                    },
                    { arr: [] }
                  )
                  .arr.map(formatKGToTonnes)
              )
            },
            {
              name: activeYear - 1,
              type: "line",
              data: [0].concat(
                annualBlocksLastYear
                  .reduce(
                    (result, month) => {
                      let last = result.arr[result.arr.length - 1] || 0;
                      return {
                        arr: [].concat(result.arr, [last + month.totalCarbon])
                      };
                    },
                    { arr: [] }
                  )
                  .arr.map(formatKGToTonnes)
              )
            }
          ]
        }
      },
      {
        title: "Cost",
        xLabel: "Time (Month)",
        yLabel: "Cost (£)",
        tooltipFormat: params =>
          `£${numberWithCommas(formatTo3DP(params.value * 1000))} for ${
            params.name
          } ${params.seriesName}`,
        chart: {
          xAxis: {
            axisPointer: {
              value: MONTHS[latestMonth].substring(0, 3)
            }
          },
          series: [
            {
              name: activeYear,
              type: "line",
              data: [0].concat(
                annualBlocks
                  .reduce(
                    (result, month) => {
                      let last = result.arr[result.arr.length - 1] || 0;
                      return {
                        arr: [].concat(result.arr, [last + month.totalCost])
                      };
                    },
                    { arr: [] }
                  )
                  .arr.map(cost => cost / 1000)
              )
            },
            {
              name: activeYear - 1,
              type: "line",
              data: [0].concat(
                annualBlocksLastYear
                  .reduce(
                    (result, month) => {
                      let last = result.arr[result.arr.length - 1] || 0;
                      return {
                        arr: [].concat(result.arr, [last + month.totalCost])
                      };
                    },
                    { arr: [] }
                  )
                  .arr.map(cost => cost / 1000)
              )
            }
          ]
        }
      }
    ],
    totalToDate: {
      value: formatTo3DP(formatKGToTonnes(annualSummary.total.totalCarbon)) > 0 ? formatTo3DP(formatKGToTonnes(annualSummary.total.totalCarbon)) : formatTo3DP(annualSummary.total.totalCarbon),
      unit: formatTo3DP(formatKGToTonnes(annualSummary.total.totalCarbon)) > 0 ? "tonnes" : "kg",
      tooltip: `That's ${nycFlightEstimate(
        formatKGToTonnes(annualSummary.total.totalCarbon)
      )}!`,
      monthlyChange: {
        value: getChange(
          annualSummary.total.totalCarbon,
          annualSummary.total.totalCarbon - latestMonthData.total.totalCarbon
        ),
        unit: "%",
        period: "month"
      },
      yearlyChange: {
        value: getChange(annualSummary.total.totalCarbon, annualSummaryLastYear.total.totalCarbon),
        unit: "%",
        period: "year"
      }
    },
    totalCostToDate: {
      value: "£" + numberWithCommas(annualSummary.total.totalCost),
      unit: "",
      tooltip: "£" + numberWithCommas(annualSummary.total.totalCost),
      monthlyChange: {
        value: getChange(
          annualSummary.total.totalCost,
          annualSummary.total.totalCost - latestMonthData.total.totalCost
        ),
        unit: "%",
        period: "month"
      },
      yearlyChange: {
        value: getChange(annualSummary.total.totalCost, annualSummaryLastYear.total.totalCost),
        unit: "%",
        period: "year"
      }
    },
    previousMonth: {
      value: formatTo3DP(formatKGToTonnes(latestMonthData.total.totalCarbon)) > 0 ? formatTo3DP(formatKGToTonnes(latestMonthData.total.totalCarbon)) : formatTo3DP(latestMonthData.total.totalCarbon),
      unit: formatTo3DP(formatKGToTonnes(latestMonthData.total.totalCarbon)) > 0 ? "tonnes" : "kg",
      tooltip: `That's ${nycFlightEstimate(
        formatKGToTonnes(latestMonthData.total.totalCarbon)
      )}!`,
      monthlyChange: {
        value: getChange(
          latestMonthData.total.totalCarbon,
          priorMonthData.total.totalCarbon
        ),
        unit: "%",
        period: "month"
      },
      yearlyChange: {
        value: getChange(
          latestMonthData.total.totalCarbon,
          latestMonthDataLastYear.total.totalCarbon
        ),
        unit: "%",
        period: "year"
      }
    },
    previousMonthCost: {
      value: "£" + numberWithCommas(latestMonthData.total.totalCost),
      unit: "",
      tooltip: "£" + numberWithCommas(latestMonthData.total.totalCost),
      monthlyChange: {
        value: getChange(
          latestMonthData.total.totalCost,
          priorMonthData.total.totalCost
        ),
        unit: "%",
        period: "month"
      },
      yearlyChange: {
        value: getChange(
          latestMonthData.total.totalCost,
          latestMonthDataLastYear.total.totalCost
        ),
        unit: "%",
        period: "year"
      }
    },
    totalTravellers: {
      value: totalTravellers,
      monthlyChange: {
        unit: "%",
        value: getChange(
          countTravellers(
            [].concat(
              latestMonthData.hotel.employees || [],
              latestMonthData.train.employees || [],
              latestMonthData.taxi.employees || [],
              latestMonthData.flight.employees || [],
              latestMonthData.car.employees || []
            )
          ),
          countTravellers(
            [].concat(
              priorMonthData.hotel.employees || [],
              priorMonthData.train.employees || [],
              priorMonthData.taxi.employees || [],
              priorMonthData.flight.employees || [],
              priorMonthData.car.employees || []
            )
          )
        ),
        period: "month"
      },
      project: {
        title: "Nationwide ERP Strategy",
        value: "110",
        difference: 20
      },
      previous: latestMonthDataLastYear.total.employees && getTotalTravellers(getUniqueTravellers(latestMonthDataLastYear.total.employees)),
      overview: [
        {
          title: "Traveller Overview",
          xLabel: "Time (Month)",
          yLabel: "No. of Travellers",
          tooltipFormat: params =>
            `${params.value} travellers for ${params.name} ${
              params.seriesName
            }`,
          chart: {
            xAxis: {
              axisPointer: {
                value: MONTHS[latestMonth].substring(0, 3)
              }
            },
            series: [
              {
                name: activeYear,
                type: "line",
                data: [0].concat(
                  annualBlocks.map(block => countTravellers(block.employees))
                )
              },
              {
                name: activeYear - 1,
                type: "line",
                data: [0].concat(
                  annualBlocksLastYear.map(block => countTravellers(block.employees))
                )
              }
            ]
          }
        }
      ]
    },
    totalTrips: {
      value:
        latestMonthData.total.numberOfTrips ||
        latestMonthData.total.numberOfReturns ||
        latestMonthData.total.bookingCount,
      monthlyChange: {
        unit: "%",
        value: getChange(
          latestMonthData.total.numberOfTrips,
          priorMonthData.total.numberOfTrips
        ),
        period: "month"
      },
      totalToDate:
        annualSummary.total.numberOfTrips ||
        annualSummary.total.numberOfReturns,
      routes: Object.values((
        (annualSummary.total.routes &&
          annualSummary.total.routes
            .map(d => ({
              from: d.origin || "Unknown",
              to: d.destination,
              count: d.count
            }))) ||
        (annualSummary.total.destinations &&
          annualSummary.total.destinations
            .filter(d => d.incurredLocation)
            .map(d => ({
              from: "Unknown",
              to: d.incurredLocation,
              count: d.count
            }))) ||
        []
      )
      .reduce((out, val) => Object.assign({}, out, {
        [val.from + "-:-" + val.to]: Object.assign({}, out[val.from + "-:-" + val.to] || {}, val, {
          count: ((out[val.from + "-:-" + val.to] || {}).count || 0) + val.count
        })
      }), {}))
      .sort((a, b) => b.count - a.count)
      .map(route => 
        route.from && route.to
          ? `${route.from} to ${route.to} - ${
              route.count
            } trips`
          : route.to ? `Unknown to ${route.to} - ${route.count} trips` : `Unknown Route - ${route.count} trips`
      )
      .splice(0, 3),
      overview: [
        {
          title: "Trip Overview",
          xLabel: "Time (Month)",
          yLabel: "No. of Trips",
          tooltipFormat: params =>
            `${params.value} trips for ${params.name} ${params.seriesName}`,
          chart: {
            xAxis: {
              axisPointer: {
                value: MONTHS[latestMonth].substring(0, 3)
              }
            },
            series: [
              {
                name: activeYear,
                type: "line",
                data: [0, ...annualBlocks.map(block => block.numberOfTrips)]
              },
              {
                name: "Train",
                type: "line",
                data: [0, ...trainBlocks.map(block => block.numberOfTrips)],
                indent: true
              },
              {
                name: "Taxi",
                type: "line",
                data: [0, ...taxiBlocks.map(block => block.numberOfTrips)],
                indent: true
              },
              {
                name: "Car",
                type: "line",
                data: [0, ...carBlocks.map(block => block.numberOfTrips)],
                indent: true
              },
              {
                name: "Air",
                type: "line",
                data: [0, ...flightBlocks.map(block => block.numberOfTrips)],
                indent: true
              },
              {
                name: activeYear - 1,
                type: "line",
                data: [
                  0,
                  ...annualBlocksLastYear.map(block => block.numberOfTrips)
                ]
              },
              {
                name: "Train",
                type: "line",
                data: [
                  0,
                  ...trainBlocksLastYear.map(block => block.numberOfTrips)
                ],
                indent: true
              },
              {
                name: "Taxi",
                type: "line",
                data: [
                  0,
                  ...taxiBlocksLastYear.map(block => block.numberOfTrips)
                ],
                indent: true
              },
              {
                name: "Car",
                type: "line",
                data: [
                  0,
                  ...carBlocksLastYear.map(block => block.numberOfTrips)
                ],
                indent: true
              },
              {
                name: "Air",
                type: "line",
                data: [
                  0,
                  ...flightBlocksLastYear.map(block => block.numberOfTrips)
                ],
                indent: true
              }
            ]
          }
        }
      ],
      maps: {
        train: ((latestMonthData.train &&
          latestMonthData.train &&
          latestMonthData.train.routes &&
          latestMonthData.train.routes.sort((a, b) => b.count - a.count)) ||
          [])[0],
        flight: ((latestMonthData.flight &&
          latestMonthData.flight.routes &&
          latestMonthData.flight.routes.sort((a, b) => b.count - a.count)) ||
          [])[0]
      },
      detail: {
        train: [
          new Statistic(
            "No. of travellers travelling by train",
            latestMonthData.train &&
            latestMonthData.train.employees &&
            latestMonthData.train.employees.filter(
              employee => employee.employeeId && employee.employeeId.length > 0
            ).length > 0
              ? latestMonthData.train.employees.filter(
                  employee => employee.employeeId.length > 0
                ).length
              : "-"
          ),
          new Statistic(
            "No. of trips taken by train",
            (latestMonthData.train &&
              (latestMonthData.train.numberOfTrips ||
                latestMonthData.train.numberOfReturns)) ||
              "-"
          ),
          new Statistic(
            "Top 5 Routes",
            ((latestMonthData.train &&
              latestMonthData.train &&
              latestMonthData.train.routes &&
              latestMonthData.train.routes
                .sort((a, b) => b.count - a.count)
                .map(route =>
                  route.origin && route.destination
                    ? `${route.origin} to ${route.destination} - ${
                        route.count
                      } trips`
                    : `Unknown Route - ${route.count} trips`
                )) ||
              []).splice(0, 5)
          ),
          new Statistic(
            "Total Mileage",
            latestMonthData.train.totalDistance &&
              formatTo3DP(latestMonthData.train.totalDistance) + " miles"
          ),
          new Statistic(
            "Average Trip Mileage",
            latestMonthData.train.averageDistance &&
              formatTo3DP(latestMonthData.train.averageDistance) + " miles"
          ),
          new Statistic(
            "Average Time on Train",
            latestMonthData.train.averageDistance &&
              formatTime(latestMonthData.train.averageDistance / 62.5)
          ),
          new Statistic(
            "Total Carbon Emissions",
            latestMonthData.train.totalCarbon &&
              (formatTo3DP(formatKGToTonnes(latestMonthData.train.totalCarbon)) > 0 ? formatTo3DP(formatKGToTonnes(latestMonthData.train.totalCarbon)) +
                " tonnes" : formatTo3DP(latestMonthData.train.totalCarbon) +
                " kg")
          ),
          new Statistic(
            "Total cost of train travel",
            latestMonthData.train.totalCost &&
              "£" + formatTo3DP(latestMonthData.train.totalCost)
          )
        ],
        taxi: [
          new Statistic(
            "No. of travellers travelling by taxi",
            latestMonthData.taxi &&
            latestMonthData.taxi.employees &&
            latestMonthData.taxi.employees.filter(
              employee => employee.employeeId && employee.employeeId.length > 0
            ).length > 0
              ? latestMonthData.taxi.employees.filter(
                  employee => employee.employeeId.length > 0
                ).length
              : "-"
          ),
          new Statistic(
            "No. of trips taken by taxi",
            latestMonthData.taxi.numberOfTrips
          ),
          new Statistic(
            "Total Mileage",
            latestMonthData.taxi.totalDistance &&
              formatTo3DP(latestMonthData.taxi.totalDistance) + " miles"
          ),
          new Statistic(
            "Average Trip Mileage",
            latestMonthData.taxi.averageDistance &&
              formatTo3DP(latestMonthData.taxi.averageDistance) + " miles"
          ),
          new Statistic(
            "Average Journey Time",
            latestMonthData.taxi.averageDistance &&
              formatTime(latestMonthData.taxi.averageDistance / 40)
          ),
          new Statistic(
            "Total Carbon Emissions",
            latestMonthData.taxi.totalCarbon &&
              (formatTo3DP(formatKGToTonnes(latestMonthData.taxi.totalCarbon)) > 0 ? formatTo3DP(formatKGToTonnes(latestMonthData.taxi.totalCarbon)) +
                " tonnes" : formatTo3DP(latestMonthData.taxi.totalCarbon) +
                " kg")
          ),
          new Statistic(
            "Total cost of taxi travel",
            latestMonthData.taxi.totalCost &&
              "£" + formatTo3DP(latestMonthData.taxi.totalCost)
          )
        ],
        car: [
          new Statistic(
            "No. of travellers travelling by car",
            latestMonthData.car &&
            latestMonthData.car.employees &&
            latestMonthData.car.employees.filter(
              employee => employee.employeeId && employee.employeeId.length > 0
            ).length > 0
              ? latestMonthData.car.employees.filter(
                  employee => employee.employeeId.length > 0
                ).length
              : "-"
          ),
          new Statistic(
            "No. of trips taken by car",
            latestMonthData.car.numberOfTrips
          ),
          new Statistic(
            "Total Mileage",
            latestMonthData.car.totalDistance &&
              formatTo3DP(latestMonthData.car.totalDistance) + " miles"
          ),
          new Statistic(
            "Average Trip Mileage",
            latestMonthData.car.averageDistance &&
              formatTo3DP(latestMonthData.car.averageDistance) + " miles"
          ),
          new Statistic(
            "Average Journey Time",
            latestMonthData.car.averageDistance &&
              formatTime(latestMonthData.car.averageDistance / 40)
          ),
          new Statistic(
            "Total Carbon Emissions",
            latestMonthData.car.totalCarbon &&
              (formatTo3DP(formatKGToTonnes(latestMonthData.car.totalCarbon)) > 0 ? 
              formatTo3DP(formatKGToTonnes(latestMonthData.car.totalCarbon)) + " tonnes" : 
              formatTo3DP(latestMonthData.car.totalCarbon) + " kg")
          ),
          new Statistic(
            "Total cost of car travel",
            latestMonthData.car.totalCost &&
              "£" + formatTo3DP(latestMonthData.car.totalCost)
          )
        ],
        flight: [
          new Statistic(
            "No. of travellers travelling by aeroplane",
            latestMonthData.flight &&
            latestMonthData.flight.employees &&
            latestMonthData.flight.employees.filter(
              employee => employee.employeeId && employee.employeeId.length > 0
            ).length > 0
              ? latestMonthData.flight.employees.filter(
                  employee => employee.employeeId.length > 0
                ).length
              : "-"
          ),
          new Statistic(
            "No. of trips taken by aeroplane",
            (latestMonthData.flight &&
              (latestMonthData.flight.numberOfTrips ||
                latestMonthData.flight.numberOfReturns)) ||
              "-"
          ),
          new Statistic(
            "Top 5 Routes",
            (
              (latestMonthData.flight &&
                latestMonthData.flight &&
                latestMonthData.flight.routes &&
                latestMonthData.flight.routes
                  .sort((a, b) => b.count - a.count)
                  .map(route =>
                    route.origin && route.destination
                      ? `${route.origin} to ${route.destination} - ${
                          route.count
                        } trips`
                      : `Unknown Route - ${route.count} trips`
                  )) ||
              []
            ).splice(0, 5)
          ),
          new Statistic(
            "Total Mileage",
            latestMonthData.flight.totalDistance &&
              formatTo3DP(latestMonthData.flight.totalDistance) + " miles"
          ),
          new Statistic(
            "Average Trip Mileage",
            latestMonthData.flight.averageDistance &&
              formatTo3DP(latestMonthData.flight.averageDistance) + " miles"
          ),
          new Statistic(
            "Average Flight Time",
            latestMonthData.flight.averageDistance &&
              formatTime(
                (latestMonthData.flight.averageDistance - 100) / 621 + 1
              )
          ),
          new Statistic(
            "Total Carbon Emissions",
            latestMonthData.flight.totalCarbon &&
              (formatTo3DP(
                formatKGToTonnes(latestMonthData.flight.totalCarbon)
              ) > 0 ? formatTo3DP(
                formatKGToTonnes(latestMonthData.flight.totalCarbon)
              ) + " tonnes" : formatTo3DP(
              ) + " kg")
          ),
          new Statistic(
            "Total cost of air travel",
            latestMonthData.flight.totalCost &&
              "£" + formatTo3DP(latestMonthData.flight.totalCost)
          )
        ]
      }
    },
    totalAccommodation: {
      value: latestMonthData.hotel.numberOfNights,
      monthlyChange: {
        unit: "%",
        value: getChange(
          latestMonthData.hotel.numberOfNights,
          priorMonthData.hotel.numberOfNights
        ),
        period: "month"
      },
      totalToDate: hotelSummary.total.numberOfNights,
      travellers: `${countTravellers(
        latestMonthData.hotel.employees
      )} travellers (${formatTo3DP(
        (countTravellers(latestMonthData.hotel.employees) / totalTravellers) *
          100
      )}% of travellers)`,
      change: {
        unit: "%",
        value: getChange(
          countTravellers(latestMonthData.hotel.employees),
          countTravellers(priorMonthData.hotel.employees)
        ),
        period: "month"
      },
      reasons:
        Object.values((latestMonthData.hotel.reasonsForTravel ||
        [])
        .reduce((out, val) => Object.assign({}, out, {
          [val.travelReason]: Object.assign({}, out[val.travelReason] || {}, val, {
            count: ((out[val.travelReason] || {}).count || 0) + val.count
          })
        }), {}))
        .sort((a, b) => b.count - a.count)
        .map(
          reason =>
            `${
              reason.travelReason && reason.travelReason.length > 0
                ? reason.travelReason
                : "Unknown"
            } - ${reason.count} trips`
        )
        .splice(0, 5),
      locations:
        Object.values((latestMonthData.hotel.hotelNames || [])
          .reduce((out, val) => Object.assign({}, out, {
            [val.hotelName]: Object.assign({}, out[val.hotelName] || {}, val, {
              count: ((out[val.hotelName] || {}).count || 0) + val.count
            })
          }), {}))
          .sort((a, b) => b.count - a.count)
          .map(
            hotel =>
              `${hotel.hotelName || "Unknown Hotel"} - ${hotel.count} bookings`
          )
          .splice(0, 5),
      overview: [
        {
          title: "Average Nights Away Overview",
          xLabel: "Time (Month)",
          yLabel: "Average No. of Nights Away",
          tooltipFormat: params =>
            `${params.value} Nights Away for ${params.name} ${
              params.seriesName
            }`,
          chart: {
            xAxis: {
              axisPointer: {
                value: MONTHS[latestMonth].substring(0, 3)
              }
            },
            series: [
              {
                name: activeYear,
                type: "line",
                data: [0].concat(hotelBlocks.map(block => block.numberOfNights))
              },
              {
                name: activeYear - 1,
                type: "line",
                data: [0].concat(
                  hotelBlocksLastYear.map(block => block.numberOfNights)
                )
              }
            ]
          }
        }
      ]
    },
    totalCost: {
      value: numberWithCommas(formatTo3DP(latestMonthData.total.totalCost)),
      monthlyChange: {
        unit: "%",
        value: getChange(
          latestMonthData.total.totalCost,
          priorMonthData.total.totalCost
        ),
        period: "month"
      },
      reasons:
        latestMonthData.total.reasonsForTravel &&
        Object.values(
          latestMonthData.total.reasonsForTravel.reduce(
            (out, r) =>
              Object.assign({}, out, {
                [r.travelReason]: {
                  travelReason: r.travelReason,
                  count:
                    r.count +
                    ((out[r.travelReason] && out[r.travelReason].count) || 0)
                }
              }),
            {}
          )
        )
          .sort((a, b) => b.count - a.count)
          .splice(0, 5)
          .map(
            reason =>
              `${
                reason.travelReason && reason.travelReason.length > 0
                  ? reason.travelReason
                  : "Unknown"
              } - ${reason.count} travellers`
          ),
      breakdown: [
        {
          mode: "Aeroplane",
          value: latestMonthData.flight.totalCost,
          distribution: Math.round(
            (countTravellers(latestMonthData.flight.employees || []) /
              (totalTravellers === 0 ? 1 : totalTravellers)) *
              100
          )
        },
        {
          mode: "Taxi",
          value: latestMonthData.taxi.totalCost,
          distribution: Math.round(
            (countTravellers(latestMonthData.taxi.employees || []) /
              (totalTravellers === 0 ? 1 : totalTravellers)) *
              100
          )
        },
        {
          mode: "Car",
          value: latestMonthData.car.totalCost,
          distribution: Math.round(
            (countTravellers(latestMonthData.car.employees || []) /
              (totalTravellers === 0 ? 1 : totalTravellers)) *
              100
          )
        },
        {
          mode: "Hotel",
          value: latestMonthData.hotel.totalCost,
          distribution: Math.round(
            (countTravellers(latestMonthData.hotel.employees || []) /
              (totalTravellers === 0 ? 1 : totalTravellers)) *
              100
          )
        },
        {
          mode: "Train",
          value: latestMonthData.train.totalCost,
          distribution: Math.round(
            (countTravellers(latestMonthData.train.employees || []) /
              (totalTravellers === 0 ? 1 : totalTravellers)) *
              100
          )
        }
      ]
        .sort((a, b) => (b.value || 0) - (a.value || 0))
        .map(
          value =>
            `${value.mode} (${value.distribution ||
              0}% of travellers) = £${(value.value && value.value.toFixed(2)) ||
              "0.00"}`
        ),
      expensive: [
        "London to Krackow - 12200 miles - £450 - By Aeroplane",
        "London to Swindon - 330 miles - £60 - By Train",
        "London to Clapham - 60 miles - £40 - By Taxi",
        "London to Swindon - 330 miles - £60 - By Train",
        "London to Clapham - 60 miles - £40 - By Taxi"
      ],
      overview: [
        {
          title: "Cost Overview",
          xLabel: "Time (Month)",
          yLabel: "Cost (£)",
          tooltipFormat: params =>
            `£${numberWithCommas(formatTo3DP(params.value))} for ${
              params.name
            } ${params.seriesName}`,
          chart: {
            xAxis: {
              axisPointer: {
                value: MONTHS[latestMonth].substring(0, 3)
              }
            },
            series: [
              {
                name: activeYear,
                type: "line",
                data: [0].concat(annualBlocks.map(block => block.totalCost))
              },
              {
                name: activeYear - 1,
                type: "line",
                data: [0].concat(
                  annualBlocksLastYear.map(block => block.totalCost)
                )
              }
            ]
          }
        }
      ]
    },
    employees:
      Object.entries(uniqueTravellers).map(entry =>
        Object.assign({}, entry[1], {
          employeeId: entry[0]
        })
      ) || [],
    header: {
      topLocation: (
        latestMonthData.total.destinations
          .filter(i => i && i.incurredLocation && i.incurredLocation.length > 0)
          .sort((a, b) => b.count - a.count)[0] || {}
      ).incurredLocation,
      activeYear,
      latestMonth,
      availableMonths,
      lastModified: aggregates.lastModified
    },
    aggregates
  };
};

export default aggregatesToData;