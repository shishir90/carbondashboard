const proxy = require('http-proxy-middleware');

const config = {
  "/.auth": {
    "target": "http://localhost:1756"
  },
  "/api": {
    "target": "http://localhost:7071"
  }
};
module.exports = (app) => {
  Object.entries(config).map(entry => app.use(proxy(entry[0], entry[1])));
}