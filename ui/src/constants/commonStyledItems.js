import styled from "styled-components"
import { colors } from 'settings'

export const StyledButton = styled.button`
  min-width: ${props => props.small ? '0':'120px'};;
  box-sizing: border-box;
  font-size: ${props => props.small ? '14px':'15px'};;
  border-radius: 20px;
  border: solid 1px ${props => props.inactive ? colors.baseColorLight : colors.baseColor};
  color: ${props => props.inactive ? colors.baseColorLight : colors.baseColor};
  background: transparent;
  padding: ${props => props.small ? '5px 15px':'9px 20px'};
  position: relative;
  text-transform: ${props => props.small ? 'normal':'uppercase'};
  text-decoration: none;
  cursor: pointer;
  transition: background-color .2s, color .2s;
  outline: none;
  text-align: center;
  background-color: #fff;
  display: inline-block;

  &:hover {
    background-color: ${props => props.inactive ? colors.baseColorLight : colors.baseColor};
    color: #fff;
  }
`;

