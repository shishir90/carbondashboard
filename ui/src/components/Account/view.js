import React from "react";

import DataView from 'components/DataView'

import PaginatedListing, { Field } from "../PaginatedListing";

export default (props) => <DataView {...props}>
  {props.data && props.data.latestMonth && <PaginatedListing
   link={"/projects"}
   title="Project Overview"
   getData={props.getProjects}
   fields={[
    new Field("_id", "Code"),
    new Field("description", "Project Name"),
    new Field("summary.avgNightsAway", "Average No. of Nights Away", "avg nights away"),
    new Field("summary.annualCost", "Total cost of travel (year)", "annual cost"),
    new Field("summary.monthCost", "Total cost of travel (month)", "monthly cost"),
    new Field("summary.carbonPerHead", "Carbon Per Head (month)", "carbon per head"),
    new Field("summary.costPerHead", "Cost Per Head (month)", "average cost per head"),
  ]}
  searchable />}

</DataView>;