import React, { useState, useEffect } from 'react'
import View from './view'

import AccountAPI from "../../api/account";
import aggregatesToData from "../../utils/aggregatesToData";

const MONTHS = [
  "JANUARY",
  "FEBRUARY",
  "MARCH",
  "APRIL",
  "MAY",
  "JUNE", 
  "JULY",
  "AUGUST",
  "SEPTEMBER",
  "OCTOBER",
  "NOVEMBER",
  "DECEMBER"
];

const Account = props => {
  const [data, updateData] = useState({})
  const [activeTab, changeTab] = useState(null)

  const [metadata, updateMetadata] = useState({
    name: props.match.params.id
  });
  let activeMonth, setActiveMonth, dropdownOptions;
  if (data && data.header) {
    dropdownOptions = data.header.availableMonths.map(month => ({
      value: month,
      label: `${MONTHS[month]} ${data.header.activeYear}`
    }));
    [activeMonth, setActiveMonth] = useState({
      value: data.header.latestMonth,
      label: `${MONTHS[data.header.latestMonth]} ${data.header.activeYear}`
    });
  }

  const storeActiveMonth = month => {
    setActiveMonth(month)
    updateData(Object.assign({}, data, aggregatesToData(data.aggregates, month.value)));
  };

  useEffect(() => {
    let accountId = props.match.params.id;
    AccountAPI.getAccount(accountId)
      .then(result => {
        updateMetadata(result);
        updateData(Object.assign({}, data, aggregatesToData(result.aggregations && result.aggregations[0])));
      });
  }, [props.match.params.id])

  const getProjects = (startIndex, count, sortField, search, includeCount) => {
    let accountId = props.match.params.id;
    return AccountAPI.getProjects(accountId, startIndex, count, sortField.value, search, data.activeYear + "-" + data.latestMonth, includeCount)
      .then(result => result)
      // .catch(console.error);
  }

  return <View {...props} activeMonth={activeMonth} getProjects={getProjects} setActiveMonth={storeActiveMonth} data={data} dropdownOptions={dropdownOptions || []} metadata={metadata} activeTab={activeTab} updateTab={changeTab} />
}

export default Account
