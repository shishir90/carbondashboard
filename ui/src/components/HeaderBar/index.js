import styled from 'styled-components'
import React from 'react'

const Header = styled.header`
  background: #fff;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.25);
  box-sizing: border-box;
  display: flex;
  justify-content: flex-end;
  height: 70px;
  padding: 0.6em;
  align-items: center;
  z-index: 1;
`

export default () => <Header>
  <img src={require("../../images/icons/profile.svg")} alt="my profile" />
</Header>