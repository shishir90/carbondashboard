import React from 'react'
import styled from 'styled-components'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NavBar from 'components/NavBar'
import Account from 'components/Account'
import Accounts from 'components/Accounts'
import SettingsPage from 'components/SettingsPage'
import Project from 'components/Project'
import { Snackbar } from 'components/Snackbar'


const AppWrap = styled.div`
  display: flex;
  min-width: 1024px;
  & > main {
    flex: 1;
    height: 100vh;
    display: flex;
    flex-direction: column;
  }
`;

const Page = styled.div`
  flex: 1;
  overflow: auto;
  -webkit-overflow-scrolling: touch;
  padding: 3em;

  @media (max-width: 1280px) {
    padding: 20px;
  }
  
  & > div {
    max-width: 1390px;
    margin: 0 auto;
  }
`

export default () => <Router>
  <AppWrap>
    <NavBar />
    <main>
      <Page>
        <div>
          <Switch>
            <Route path="/settings" component={SettingsPage} />
            <Route path="/" exact component={Accounts} />
            <Route path="/accounts/:id" exact component={Account} />
            <Route path="/projects/:id" exact component={Project} />
          </Switch>
        </div>
      </Page>
    </main>
    <Snackbar />
  </AppWrap>
</Router>;
