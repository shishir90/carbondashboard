import React, { useState, useEffect } from "react";

export default ({ place }) => {
    const [resolvedLocation, setResolvedLocation] = useState(null);

    useEffect(() => {
        try {
            const geocoder = new window.google.maps.Geocoder();
            geocoder.geocode({
                "address": place
            }, (raw) => {
                if (raw[0] && raw[0].formatted_address) {
                    setResolvedLocation(raw[0].formatted_address);
                } else {
                    setResolvedLocation(place);
                }
            });
        } catch (e) {
            // suppress....
            setResolvedLocation(place);
        }
    }, []);
    return <span>{resolvedLocation}</span>
};