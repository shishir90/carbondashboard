import React, { Fragment } from "react";

import CSVUploadForm from 'components/CSVUploadForm';

export default () => <Fragment>
  <h1>Settings</h1>
  <CSVUploadForm />
</Fragment>