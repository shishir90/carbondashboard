import styled from 'styled-components'

const types = {
  default: `
    @media (max-width: 1499px) {
      display: flex;
      flex-wrap: wrap;
      margin-right: -20px;

      & > * {
        box-sizing: border-box;
        width: calc(50% - 20px);
        margin: 0 20px 20px 0
      }

      & > *:first-child {
        width: 100%;
      }
    }

    @media (min-width: 1500px) {
      display: grid;
      grid-template-columns: 1fr 20px 1fr;
      grid-template-rows: 1fr 20px 1fr;
      margin-bottom: 20px;

      & > *:first-child {
        grid-column: 1/span 1;
        grid-row: 1/span 3;
      }

      & > *:nth-child(2) {
        grid-column: 3;
        grid-row: 1;
      }

      & > *:nth-child(3) {
        grid-column: 3;
        grid-row: 3;
      }
    }
  `,
  columns: `
    display: flex;
    margin-right: -20px;

    & > * {
      margin: 0 20px 20px 0;
      width: calc(25% - 20px);
      box-sizing: border-box;
    }
  `,
  split: `
    display: flex;
    margin-right: -20px;
    align-items: flex-start;

    & > * {
      margin: 0 20px 20px 0;
      width: calc(25% - 20px);
      box-sizing: border-box;
    }

    & > *:last-child {
      width: calc(75% - 20px);
      box-sizing: border-box;
    }

    @media (max-width: 1499px) {
      flex-direction: column;
      margin-right: 0;
      

      & > * {
        width: 100%;
        margin-right: 0
        &:first-child {
          order: 1;
        }
        &:last-child {
          width: 100%;
        }
      }
    }
  `
}

const Grid = styled.div`
  ${props => props.type ? types[props.type] : types.default}
`

export default Grid