import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { colors } from 'settings'

import GeocodedLocation from "../GeocodedLocation";

const Container = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  margin-bottom: 20px;
`

const Title = styled.h1`
  font-size: 24px;
  font-weight: 500;
  margin: 0;
`

const Sub = styled.div`
  display: flex;
  align-items: center;
`

const Location = styled.p`
  margin-right: 20px;
  display: flex;
  align-items: center;
  font-size: 15px;
  color: ${colors.baseColorMedium}

  & img {
    margin-right: 10px;
  }
`

const Date = styled.p`
  font-size: 15px;
  color: ${colors.baseColorLight}
`

const PageHeader = ({title, location, date, children}) => (
  <Container>
      <div>
        <Title>{title}</Title>
        {(location || date) &&
          <Sub>
            {location &&
              <Location>
                <img src={require('images/icons/pin.svg')} alt=""/>
                <GeocodedLocation place={location} />
              </Location>
            }
            {date &&
              <Date>{date}</Date>
            }
          </Sub>
        }
      </div>
      {children}
  </Container>
)

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  location: PropTypes.string,
  date: PropTypes.string
}

export default PageHeader