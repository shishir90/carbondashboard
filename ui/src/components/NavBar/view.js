import styled, { css } from 'styled-components'
import React from 'react'
import { Link } from 'react-router-dom'

const NAV = [
  {
    link: "/",
    icon: require("../../images/icons/dashboard.svg")
  },
  {
    link: "/projects",
    icon: require("../../images/icons/folder.svg")
  },
  {
    link: "/settings",
    icon: require("../../images/icons/cog.svg")
  }
]

const Nav = styled.nav`
  background: #2b0a3d;
  box-sizing: border-box;
  height: 100vh;
  min-width: 84px;
  padding-top: 2em;

  @media (max-width: 1280px) {
    min-width: 64px;
  }
`

const NavList = styled.ul`
  &, & li {
    display: block;
    padding: 1em 0;
    width: 100%;
    position: relative;
  }
  & li {
    display: flex;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;
    margin: 1.5em 0;
    text-align: center;
    height: 3.5em;
    opacity: 0.6;
    transition: 0.25s opacity ease-in-out, transform 0.5s ease-in-out;
  }
  & span.slider {
    content: "";
    display: block;
    position: absolute;
    background: #fff;
    height: 3.5em;
    width: 3px;
    right: 0;
    top: 0;
    transition: transform 0.5s ease-in-out;
    transform: translateY(${({active}) => (active * 5) + 2.5}em);
  }
  ${NAV.map((item, index) => css`
    & a.index-${index}:hover ~ span.slider {
      transform: translateY(${(index * 5) + 2.5}em);
    }
  `)}
  & a.index-${({active}) => active} li {
    opacity: 1;
  }
`

export default ({ active }) => <Nav>
  <NavList active={active}>
    {NAV.map((item, index) => <Link className={`index-${index}`} to={item.link} key={index}><li><img src={item.icon} alt="Icon" /></li></Link>)}
    <span className="slider"></span>
  </NavList>
</Nav>