import React from 'react'
import { withRouter } from 'react-router-dom'
import View from './view'

const locations = ["/", "/projects", "/settings"]

export default withRouter(({
  location
}) => <View active={locations.indexOf(location.pathname) > -1 ? locations.indexOf(location.pathname) : 0} />)