import React from "react";

import DataView from 'components/DataView'
import PaginatedListing, { Field } from "../PaginatedListing";

export default (props) => <DataView {...props}>
  {props.data && props.data.employees && <PaginatedListing
    title="Employee Overview"
    getData={props.getEmployees}
    fields={[
      new Field("employeeId", "Employee ID"),
      new Field("count", "No. Of Trips", "Trip count"),
      new Field("avgNights", "Average No. Of Nights Away", "nights away"),
      new Field("mostCommonModeOfTransport", "Most Common Mode of Transport"),
      new Field("avgDistance", "Average Distance Travelled", "distance"),
      new Field("avgCarbon", "Average Carbon Emissions Per Trip", "average carbon"),
      new Field("totalCarbon", "Carbon Emissions for this month", "total carbon"),
      new Field("totalCost", "Total Cost Per Month", "cost")
    ]} />}
</DataView>;