import React, { useState, useEffect } from 'react'
import View from './view';
import ProjectAPI from "../../api/project";
import aggregatesToData from "../../utils/aggregatesToData";

const MONTHS = [
  "JANUARY",
  "FEBRUARY",
  "MARCH",
  "APRIL",
  "MAY",
  "JUNE", 
  "JULY",
  "AUGUST",
  "SEPTEMBER",
  "OCTOBER",
  "NOVEMBER",
  "DECEMBER"
];

const SEARCH_FIELDS = ["employeeId"];

const getByPath = (obj, path) => path.split(".").reduce((o, p) => o && o[p], obj);

const Project = props => {
  const [data, updateData] = useState({});
  const [metadata, updateMetadata] = useState({
    name: props.match.params.id
  });
  const [activeTab, changeTab] = useState(null);
  let activeMonth, setActiveMonth, dropdownOptions;
  if (data && data.header) {
    dropdownOptions = data.header.availableMonths.map(month => ({
      value: month,
      label: `${MONTHS[month]} ${data.header.activeYear}`
    }));
    [activeMonth, setActiveMonth] = useState({
      value: data.header.latestMonth,
      label: `${MONTHS[data.header.latestMonth]} ${data.header.activeYear}`
    });
  }

  const storeActiveMonth = month => {
    setActiveMonth(month)
    updateData(Object.assign({}, data, aggregatesToData(data.aggregates, month.value)));
  };

  useEffect(() => {
    ProjectAPI.getProject(props.match.params.id)
    .then(metadata => {
      updateMetadata(metadata)
      metadata.aggregations && metadata.aggregations[0] && updateData(Object.assign({}, data, aggregatesToData(metadata.aggregations[0])));
    })
    .catch(e => 
      //Do nothing, likely just bad id/name mapping for project ID
    false);
  }, [props.match.params.id]);

  const getEmployees = (startIndex, count, sortField, search) => {
    let emp = data.employees || [];
    let searched = emp.filter(e => SEARCH_FIELDS.reduce((out, field) => out && e[field] && e[field].indexOf(search) >= 0, true))
    let sorted = searched.sort((a, b) => getByPath(b, sortField) < getByPath(a, sortField));
    return Promise.resolve({ rows: sorted || [], documentCount: emp ? emp.length : 0});
  }

  return <View {...props} activeMonth={activeMonth} getEmployees={getEmployees} setActiveMonth={storeActiveMonth} data={data} dropdownOptions={dropdownOptions || []} metadata={metadata} activeTab={activeTab} updateTab={changeTab} />
}

export default Project
