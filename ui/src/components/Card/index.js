import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'settings'

const Container = styled.div`
  background: ${({inactive}) => inactive ? colors.background : '#fff'};
  border-radius: 4px;
  box-shadow: 0 1px 1px 0 ${colors.baseColorLight};
  border: solid 1px #ebedf8;  
  box-sizing: border-box;
  padding: ${props => props.large ? '2em' : '1em'};
  position: relative;
  ${({onClick}) => onClick && 'cursor: pointer;'}
  transition: background-color .2s;

  &::before,
  &::after {
    position: absolute;
    top: 100%;
    left: 50%;
    content: "";
    display: ${({active}) => active ? 'block' : 'none'};
  }

  &::before {
    width: 16px;
    height: 16px;
    box-shadow: 1px 1px 2px 0 ${colors.baseColorLight};
    transform: rotate(45deg);
    z-index: -1;
    margin: -8px 0 0 -8px;
  }

  &::after {
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 12px 12px 0 12px;
    border-color: ${({disabled}) => disabled ? colors.background : '#fff'} transparent transparent transparent;
    margin-left: -12px;
  }
`

const Card = ({children, ...props}) => (
  <Container {...props}>
    {children}
  </Container>
)

Card.propTypes = {
  onClick: PropTypes.func,
  active: PropTypes.bool,
  disabled: PropTypes.bool
}

Card.defaultProps = {
  active: false,
  disabled: false
}

export default Card
