import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import { colors } from 'settings';
export const HeaderColumn = styled.th`
  padding: 10px 10px 10px 0;
  text-transform: uppercase;
  font-size: 10px;
  font-weight: 500;
  letter-spacing: 0.7px;
  text-align: left;
  color: ${colors.baseColorLight};
  &:first-child {
    padding-left: 10px;
  }
`;

const Header = ({ fields, link }) => (
  <thead>
    <tr>
      {fields.map((field, key) => {
        const { label } = field;
        return <HeaderColumn key={key}>{label}</HeaderColumn>;
      })}
      {link &&
        <HeaderColumn></HeaderColumn>
      }
    </tr>
  </thead>
)

Header.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired
};
export default Header;
