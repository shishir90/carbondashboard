import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import Header from './Header';
import Rows from './Row';

export const Table = styled.table`
  margin-top: 40px;
  border-collapse: collapse;
  width: 100%;
`;

const Container = ({ data, sortBy, fields, link }) => <Table>
  <Header fields={fields} link={link}/>
  <Rows data={data} sortBy={sortBy} fields={fields} link={link}/>
</Table>

Rows.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  sortBy: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired
};

export default Container;
