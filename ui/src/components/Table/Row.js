import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import { colors } from 'settings';
import {Link} from 'react-router-dom'
import { StyledButton } from 'constants/commonStyledItems'

const Body = styled.tbody`
  tr:first-child td {
    border-top: 1px solid ${colors.baseColorLight};
  }
`;

export const Row = styled.tr``

export const Column = styled.td`
  background-color: ${props => (props.code === props.sortBy) ? colors.background2 : '#ffffff'};
  color: ${props => props.color || (props.code === props.sortBy) ? colors.baseColor : colors.baseColorMedium };
  text-align: ${props => props.align || 'left'};
  padding: 10px 10px 10px 0;
  ${props => (props.code === props.sortBy) && 'padding-left: 10px'};
  ${props => (props.code === 'link') && 'padding-left: 20px'};
  font-size: 15px;
  min-width: 60px;
  ${props => props.code === 'name' && 'width: 25%;'};
  &:first-child {
    padding-left: 10px;
  }
`;

export const Value = styled.span`
  &.currency {
    ::before {
      content: '£';
    }
  }
`;

const formatTo3DP = n => Math.round(n * 100) / 100;
const formatValue = v => typeof v === "number" ? formatTo3DP(v) : v || "-";

const Cell = ({record, sortBy, fields}) => (
  <Fragment>
    {fields.map((field, key) => {
      const { align, color, name, resolveValue, type } = field;
      return <Column key={key} code={name} sortBy={sortBy} color={color} align={align}>
        <Value className={type}>{formatValue(resolveValue ? resolveValue(record) : record[name])}</Value>
      </Column>;
    })}
  </Fragment>
)

const Rows = ({data, sortBy, fields, link}) => (
  <Body>
    {data.map((record, index) => {
      return <Row key={index}>
        <Cell record={record} sortBy={sortBy} fields={fields}/>
        {link &&
          <Column code="link" align="right">
            <Link to={`${link}/${record.id || record._id}`}>
              <StyledButton small inactive>Open</StyledButton>
            </Link>
          </Column>
        }
      </Row>
    })}
  </Body>
)

Cell.propTypes = {
  record: PropTypes.shape({}).isRequired,
  sortBy: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired
    })
  ).isRequired
};

Rows.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  sortBy: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired
    }).isRequired
  )
};

export default Rows;
