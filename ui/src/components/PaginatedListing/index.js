import React, { useState } from "react";

import Listing, { Field as ListingField } from 'components/Listing'

export const Field = ListingField;

export default ({ getData, link, title, fields, searchable, ...props }) => {
    const [sortedData, setSortedData] = useState(null);
    const [count, setCount] = useState(0);
    const dataRetrieval = (startIndex, limit, sortAttribute, search, clear) => {
        if (clear) {
            setSortedData(null);
        }
        getData(startIndex, limit, sortAttribute, search, clear)
            .then(output => {
              let sorted = {};
              if (!clear && sortedData) {
                Object.assign(sorted, sortedData);
              }
              Object.assign(sorted, output.rows.reduce((out, doc, index) => Object.assign({}, out, {
                [startIndex + index]: doc
              }), {}))
              setSortedData(sorted);
              setCount(output.documentCount || count || 0);
            })
    }
    return <Listing count={count} dataRetrieval={dataRetrieval} link={link} title={title} list={sortedData && Object.values(sortedData)} fields={fields} searchable={searchable} {...props} />;
}