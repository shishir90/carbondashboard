import React from 'react';
import styled from 'styled-components';
import ReactSelect from 'react-select';
import { DropdownIndicator } from './components';
import { colors } from 'settings';

const Container = styled.div`
  display: flex;
  max-width: 400px;
`;

const Label = styled.label`
  height: 17px;
  font-size: 15px;
  letter-spacing: 0.7px;
  text-align: right;
  color: ${colors.baseColorLight};
  margin-top:12px;
  white-space: nowrap;
  text-transform: uppercase;
`

const Select = styled(ReactSelect)`
  display: inline-block;
  padding-left: 10px;
  width: 100%;
`
const selectStyles = {
  control: provided => ({
    ...provided,
    minWidth: 240,
    borderRadius: 23,
    height: 45,
    borderColor: colors.baseColorLight,
    padding: '0px 15px',
    textTransform: 'uppercase',
    fontSize: 15,
    letterSpacing: 1.4
  }),
  menu: provided => ({ ...provided, textTransform: 'uppercase' }),
  indicatorSeparator: provided => ({...provided, display: 'none'}),
  option: provided => ({...provided, padding: 15, fontSize: 15}),
};

export default ({options, change, label, value}) => <Container>
  <Label>{label}</Label>
  <Select
    components={{ DropdownIndicator }}
    options={options}
    value={value}
    isSearchable={false}
    styles={selectStyles}
    onChange={change}
  />
</Container>
