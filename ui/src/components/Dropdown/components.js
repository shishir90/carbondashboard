import React from 'react';
import styled from 'styled-components';
import { components } from 'react-select';

const DownChevron = styled.img`
  height:20px;
  width: 20px;
  content: url(${require(`images/icons/downchevron.svg`)})
`;

export const DropdownIndicator = props => {
  return (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...props}>
        <DownChevron />
      </components.DropdownIndicator>
    )
  );
};
