import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Card from 'components/Card';
import List from 'components/List';
import {ListGroup, ListGroupItem, ListGroupItemHeader, Header} from './views';
import Map from 'components/Maps';

const Container = styled(Card)`
    padding: 0px;
`;

const ArrayList = ({data, map}) => {
    return <List type='numbered' data={data} map={map}/>
}

const ListItem = ({ label, value }) => <React.Fragment>
    <ListGroupItemHeader>{label}</ListGroupItemHeader>
    <ListGroupItem>{
        value ? (typeof value === 'string' || typeof value === 'number') ? value : <ArrayList data={value}/> : "-"
    }</ListGroupItem>
</React.Fragment>;

const TripText = ({ map, mode, data }) => {
    return <Container>
        {map && <Map mode={mode} origin={map.origin} destination={map.destination}></Map>}
        <Header>Travelling by {mode}</Header>
        <ListGroup>
            {data.map(({ description, value }, index) => <ListItem key={index} label={description} value={value}/>)}
        </ListGroup>
    </Container>;
}

TripText.propTypes = {
    mode: PropTypes.string.isRequired,
    data: PropTypes.array
};

export default TripText;
