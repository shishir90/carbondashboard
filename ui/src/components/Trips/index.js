import React, { Fragment } from 'react';
import TripText from './TripText';

const Trips = ({tripData, maps}) => {
    const modes = ['train', 'taxi', 'car', 'flight'];
    return <Fragment>
        {modes.map((mode, key) => <TripText key={key} mode={mode} data={tripData[mode]} map={maps[mode]}/>)}
    </Fragment>;
}

export default Trips;