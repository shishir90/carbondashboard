import styled from 'styled-components';
import  { colors } from 'settings';

export const Header = styled.h5`
    height: 1.5em;
    font-size: 15px;
    font-weight: 500;
    color: ${colors.baseColor};
    margin-left: 29px;
    text-transform: capitalize;
`;

export const ListGroup = styled.dl`
    margin-left: 29px;
`;

export const ListGroupItemHeader = styled.dt`
    height: 1em;
    font-size: 15px;
    color: ${colors.baseColorMedium};
`;
export const ListGroupItem = styled.dd`
    font-size: 15px;
    letter-spacing: 0.2px;
    color: #354052;
    margin-inline-start: 0px;
    margin-bottom: 19px;
    margin-top: 10px;
`;
