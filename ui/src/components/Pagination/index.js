import React from 'react'
import styled from 'styled-components'
import {colors} from 'settings'

const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-top: 20px;
`
const Paginator = styled.button`
  margin-left: 20px;
  border: none;
  background: transparent;
  padding: 0;
  font-size: 0;
  width: 20px;
  height: 20px;
  position: relative;
  outline: none;
  cursor: pointer;
  & img {
    opacity: ${props => props.disabled ? '.5':'1'};
  }
`
const Range = styled.div`
  color: ${colors.baseColorLight};
  font-size: 14px;
  margin: 0;
`

const Pagination = ({range, onChange, current, total}) => (
  <Container>
    <Range>
      {range.start} - {range.end} of {range.total}
    </Range>
    <Paginator onClick={() => onChange(current - 1)} disabled={current === 1}>
      <img src={require('images/icons/left-arrow.svg')} alt=""/>
    </Paginator>
    <Paginator onClick={() => onChange(current + 1)} disabled={current === total}>
      <img src={require('images/icons/right-arrow.svg')} alt=""/>
    </Paginator>
  </Container>
)

export default Pagination