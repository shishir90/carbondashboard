import React from "react";
import styled from "styled-components";

import Button from "components/Button";
import Card from "components/Card";
import WindTurbine from "components/Loaders/WindmillSpinner";

const LoginWrapper = styled.div`
  background: #2b0a3d;
  bottom: 0;
  display: flex;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  & h1 {
    font-size: 1.5em;
  }
`;

const CardContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  & > div > .button {
    text-align: center;
    margin-top: 2em;
  }
`;

const LoadingWrapper = styled.div`
  & {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`;

export default ({ error, isLoading }) => <LoginWrapper>
  {!isLoading && <CardContainer>
    {error && <Card>
      <h1>{error.title}</h1>
      <p>{error.message}</p>
      <div className="button">
        <a href={error.getAuthUrl(!!error.customBtnText)}><Button>{error.customBtnText? error.customBtnText: "Log in with Capgemini"}</Button></a>
      </div>
    </Card>}
    {!error && <Card>
      <h1>Capgemini Carbon Dashboard</h1>
      <div className="button">
        <a href="/.auth/login/aad?post_login_redirect_url=/"><Button>Log in with Capgemini</Button></a>
      </div>
    </Card>}
  </CardContainer>}
  {isLoading && <LoadingWrapper>
    <WindTurbine />
  </LoadingWrapper>}
</LoginWrapper>;