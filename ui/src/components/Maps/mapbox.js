import mapboxStatic from '@mapbox/mapbox-sdk/services/static';
import mbxGeocoding from '@mapbox/mapbox-sdk/services/geocoding';
import arc from 'arc';

const accessToken = 'pk.eyJ1IjoiYWllIiwiYSI6ImNqMXhmYTJseTAwMGcycW8xem8yeWM0Zm4ifQ.Ld7sQDMsL90RNj5e1d8clg';

const staticClient = mapboxStatic({ accessToken });
const geocodingClient = mbxGeocoding({ accessToken });
const baseImageProps = {
    ownerId: 'aie',
    styleId: 'cjn0cmtts49862smq1rydfmqu',
    width: 500,
    height: 300,
    position: "auto"
};

const getCoordinates = ([x, y]) => {
    return {x, y};
};

const getLongLat = async ({query}) => {
    let coordinates = [];
    await geocodingClient
        .forwardGeocode({query, limit: 1})
        .send()
        .then(response => {
            coordinates = response.body.features[0].center;
        });
    return coordinates;
}

const setImageURL = (url, callback) => {
    const Image = new window.Image();
    Image.onload = function() {
        callback(this.src);
    }
    Image.src = `${url}?access_token=${accessToken}`;
}

export const generateFlightImage = async ({origin, destination, travelMode, setImageUrl, callback}) => {
    const originCoords = await getLongLat({query: origin});
    const destinationCoords = await getLongLat({query: destination});
    var generator = new arc.GreatCircle(
        getCoordinates(originCoords),
        getCoordinates(destinationCoords)
    );
    var { coords } = generator.Arc(100).geometries[0];
    return generateImageUrl({coordinates: coords, callback});
};

export const generateImageUrl = ({ coordinates, callback }) => {
    const imageProps = Object.assign({}, baseImageProps, {
        overlays: [
            {
                marker: {
                    coordinates: coordinates[0],
                    label: 'A',
                    color: '#5DA9C6',
                    size: 'large'
                }
            },
            {
                marker: {
                    coordinates: coordinates[coordinates.length - 1],
                    label: 'B',
                    color: '#7368A5',
                    size: 'large'
                },
            },
            {
                path: {
                    coordinates,
                    strokeColor: "#39628e",
                    strokeWidth: 6
                }
            }
        ]
    });

    const url = staticClient.getStaticImage(imageProps).url();
    setImageURL(url, callback)
};