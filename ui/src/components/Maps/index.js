import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import generateImageUrl from './maps';

const Image = styled.img`
    max-width: 100%;
    max-height: 100%;
`;

const travelModes = {
    train: "TRANSIT",
    car: "DRIVING",
    taxi: "DRIVING",
    flight: "FLIGHT",
}

export default props => {
    const [imageUrl, setImageUrl] = useState(require('images/map.png'));

    useEffect(() => {
      generateImageUrl({...props, callback: setImageUrl, travelMode: travelModes[props.mode]});
    }, [props.origin, props.destination, props.mode]);

    return <Image src={imageUrl} alt={`${props.origin} to ${props.destination} by ${props.mode}`}/>;
}
