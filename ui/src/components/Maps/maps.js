import polyline from '@mapbox/polyline';
import { generateFlightImage, generateImageUrl } from './mapbox';


const generateImage = ({origin, destination, travelMode, setImageUrl, callback}) => {
    const directionsService = new window.google.maps.DirectionsService();
    return directionsService.route({origin, destination, travelMode}, (response, status) => {
        if (status === 'OK') {
            const poly = response.routes[0].overview_polyline;
            const { coordinates } = polyline.toGeoJSON(poly);
            generateImageUrl({coordinates, callback});
        }
    });
};

const generatePolyLine = (props) => {
    if (props.travelMode === 'FLIGHT') {
        return generateFlightImage(props);
    }
    return generateImage(props);
};

export default props => {
    generatePolyLine(props);
};
