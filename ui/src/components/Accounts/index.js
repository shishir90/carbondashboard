import React from "react";
import View from "./view";
import MeAPI from "../../api/me";

export default () => {
    const getAccounts = (startIndex, count, sortField, search, includeCount) => {
      return MeAPI.getMe(startIndex, count, sortField.value, search, includeCount)
        .then(result => result.accounts)
        // .catch(console.error);
    }
    // useEffect(() => {
    //     MeAPI.getMe(15)
    //         .then(d => {
    //             setAccounts([].concat(d.accounts.map(a => Object.assign({}, a, { id: a._id }))));
    //             return MeAPI.getMe()
    //             .then(d => setAccounts([].concat(d.accounts.map(a => Object.assign({}, a, { id: a._id })))))    
    //         })
    //         .catch(() => {
    //             // Swallow for now.
    //         });
    // }, [])
    return <View getAccounts={getAccounts} />
}