import React from "react";
import Listing, { Field } from "../PaginatedListing";

export default ({ getAccounts }) => <Listing
  link="/accounts"
  title="My Accounts"
  getData={getAccounts}
  fields={[
    new Field("_id", "Customer ID"),
    new Field("name", "Account Name", "name")
  ]} paginate={{
      limit: 15
  }} 
  searchable />