import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'settings'

const Container = styled.p`
  font-size: 15px;
  color: ${colors.baseColorMedium};
  margin: 0;
  & strong {
      color: ${props => props.value < 0 ? colors.success : colors.error};
      font-weight: 500
  }
`

const Statistic = ({value, unit, period}) => (
  <Container value={value}>
    <strong>
      {value < 0 ? '↓':'↑'}  
      {Math.abs(value)}{unit}
    </strong> Since previous {period}
  </Container>
)

Statistic.propTypes = {
  unit: PropTypes.string.isRequired,
  period: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
}

export default Statistic