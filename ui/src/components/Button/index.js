import styled from "styled-components";

export default styled.button`
  & {
    background: transparent;
    border-radius: 20px;
    border: solid 1px var(--button-color, ${props => props.colour || "#000"});
    color: var(--button-color, ${props => props.colour || "#000"});
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    padding: 5px 25px;
    position: relative;
    line-height: 1.14;
    -webkit-font-smoothing: antialiased;
    min-width: 150px;
    outline: none;
  }
  &:hover {
    cursor: pointer;
    color: var(--button-highlight-color, ${props => props.colour || "#000"});
    border-color: var(--button-highlight-color, ${props => props.colour || "#000"});
  }
  &:active {
    background: var(--button-highlight-color, ${props => props.colour || "#000"});
    border-color: #9b9b9b;
    color: ${props => props.colour || "#fff"};
  }
`;