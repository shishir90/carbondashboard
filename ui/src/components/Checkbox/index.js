import React from 'react'
import styled from 'styled-components'
import { colors } from 'settings'

const Container = styled.div``

const Input = styled.input`
  display: none
`

const Label = styled.label`
  color: ${props => props.color ? props.color : colors.baseColorMedium};
  font-size: 14px;
  margin-left: ${props => props.indent ? '30px' : '10px'};
  display: flex;
  align-items: center;
  cursor: pointer;
`

const Box = styled.div`
  width: 22px;
  height: 22px;
  background-color: ${props => props.checked ? colors.baseColorMedium : 'transparent'};
  border: 1px solid ${colors.baseColorMedium};
  border-radius: 50%;
  margin-right: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Checkbox = ({id, onChange, checked, label, color, indent}) => (
  <Container>
    <Input type="checkbox" id={id} checked={checked} onChange={onChange}/>
    <Label indent={indent} color={color} htmlFor={id} checked={checked}>
    <Box checked={checked}>
      {checked &&
        <img src={require('images/icons/tick.svg')} alt=""/>
      }
    </Box>
    {label}</Label>
  </Container>
)

export default Checkbox