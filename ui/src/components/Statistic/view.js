import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'settings'
import Change from 'components/Change'
import Tooltip from 'components/Tooltip'

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  min-height: 110px;
  color: ${colors.baseColor};
`

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  & div {
    margin-right: 1em;
    margin-bottom: 1em;
    flex: 1;
  }
`

const Label = styled.h3`
  font-size: 15px;
  font-weight: 500;
  margin: 0;
`

const StatisticValue = styled.p`
  font-size: ${props => props.large ? '48px' : '36px'};
  margin: ${props => props.large ? '.5em 0 0' : '.3em 0 0'};
  position: relative;
`

const Changes = styled.div`
  display: flex;
  align-items: center;
  margin-top: auto;
  & p {
    flex: 1;
  }
`;

function numberWithCommas(x) {
  if (!x || x.toString().indexOf(".") >= 0) {
    return x;
  }
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function safetyCheck(n) {
  if (n + "" === "NaN" || !n) {
    return "-";
  } else {
    return n;
  }
}

const Statistic = ({ currency, large, label, value, tooltip, toggleTooltip, showTooltip, icon, monthlyChange, yearlyChange, unit}) => (
  <Container>
    <Wrapper>
      <div>
        <Label>{label}</Label>
        <StatisticValue large={large} onMouseEnter={() => toggleTooltip(true)} onMouseLeave={() => toggleTooltip(false)}>
          {currency ? "£" : ""}{safetyCheck(numberWithCommas(value))}
          {" " + (unit? unit : "")}
          {tooltip &&
            <Tooltip text={tooltip} visible={showTooltip}/>
          }
        </StatisticValue> 
      </div> 
      {icon &&
        <img src={require(`images/icons/${icon}.svg`)} alt=""/>
      }
    </Wrapper>
    {(monthlyChange || yearlyChange) &&
      <Changes>
        {monthlyChange &&
          <Change value={monthlyChange.value} period={monthlyChange.period} unit={monthlyChange.unit}/>
        }
        {yearlyChange &&
          <Change value={yearlyChange.value} period={yearlyChange.period} unit={yearlyChange.unit}/>
        }
      </Changes>
    }
  </Container>
)

Statistic.propTypes = {
  large: PropTypes.bool,
  label: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  monthlyChange: PropTypes.shape({
    value: PropTypes.number.isRequired,
    period: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired
  }),
  yearlyChange: PropTypes.shape({
    value: PropTypes.number.isRequired,
    period: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired
  }),
  tooltip: PropTypes.string,
  icon: PropTypes.oneOf(['travellers', 'flights', 'accommodation', 'cost'])
}

Statistic.defaultProps = {
  large: false,
  cardProps: {
    active: false,
    disabled: false
  }
}

export default Statistic