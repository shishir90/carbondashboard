import React, { useState } from 'react'
import View from './view'

const Statistic = props => {
  const [showTooltip, toggleTooltip] = useState(false)
  return <View {...props} showTooltip={showTooltip} toggleTooltip={toggleTooltip} />
}

export default Statistic