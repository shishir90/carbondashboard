import React, {Fragment, useState} from 'react'
import Card from 'components/Card'
import Loader from 'components/Loader'
import Grid from 'components/Grid'
import Chart from 'components/Chart'
import Statistic from 'components/Statistic'
import Change from 'components/Change'
import PageHeader from 'components/PageHeader'
import Dropdown from 'components/Dropdown'
import Trips from 'components/Trips'

const changeTab = (tab, active, updateTab) => () => {
  updateTab(active === tab ? null : tab)
}

const MONTHS = [
  "JANUARY",
  "FEBRUARY",
  "MARCH",
  "APRIL",
  "MAY",
  "JUNE", 
  "JULY",
  "AUGUST",
  "SEPTEMBER",
  "OCTOBER",
  "NOVEMBER",
  "DECEMBER"
];

function nth(d) {
  if (d > 3 && d < 21) return 'th'; 
  switch (d % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}

const properCase = sentence => sentence.split(" ").map(string => string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase()).join(" ");

const getLastModified = date => `${date.getDate()}${nth(date.getDate())} ${properCase(MONTHS[date.getMonth()])} ${date.getFullYear()}`;

const DataView = ({children, data, dropdownOptions, activeMonth, setActiveMonth, metadata, activeTab, updateTab}) => {

  const [activeGraphMode, setGraphMode] = useState(0);

  return <Fragment>
    <PageHeader title={metadata.description || metadata.name} location={data.header && data.header.topLocation} date={data.header && `Last Updated ${getLastModified(new Date(data.header.lastModified))}`}>
      {data.availableMonths && <Dropdown options={dropdownOptions} change={setActiveMonth} value={activeMonth} label='show me:'/>}
    </PageHeader>
    <Grid>
      <Card large>
        {data.overview ? (
          <Chart onActiveSetChange={setGraphMode} legendPosition="bottom" thousands data={data.overview}/>
        ):(
          <Loader/>
        )}
      </Card>
      <Card large>
        {data.totalToDate ? activeGraphMode === 0 ? (
          <Statistic
            large
            label={"Total Carbon Emissions Year to " + properCase(MONTHS[data.latestMonth]) + " " +  data.activeYear}
            unit={data.totalToDate.unit}
            value={data.totalToDate.value}
            monthlyChange={data.totalToDate.monthlyChange}
            yearlyChange={data.totalToDate.yearlyChange}
            tooltip={data.totalToDate.tooltip} />
        ) : data.totalCostToDate ? (
          <Statistic
            large
            label={"Total Cost Year to " + properCase(MONTHS[data.latestMonth]) + " " +  data.activeYear}
            unit={data.totalCostToDate.unit}
            value={data.totalCostToDate.value}
            monthlyChange={data.totalCostToDate.monthlyChange}
            yearlyChange={data.totalCostToDate.yearlyChange}
            tooltip={data.totalCostToDate.tooltip} />
        ) :(
          <Loader/>
        ) :(
          <Loader/>
        )}
      </Card>
      <Card large>
        {data.previousMonth ? activeGraphMode === 0 ? (
          <Statistic
            large
            label="Previous Month's Carbon Emissions"
            unit={data.previousMonth.unit}
            value={data.previousMonth.value}
            monthlyChange={data.previousMonth.monthlyChange}
            yearlyChange={data.previousMonth.yearlyChange}
            tooltip={data.previousMonth.tooltip} />
        ) : data.previousMonthCost ? (
          <Statistic
            large
            label="Previous Month's Cost"
            unit={data.previousMonthCost.unit}
            value={data.previousMonthCost.value}
            monthlyChange={data.previousMonthCost.monthlyChange}
            yearlyChange={data.previousMonthCost.yearlyChange}
            tooltip={data.previousMonthCost.tooltip} />
        ) :(
          <Loader/>
        ) :(
          <Loader/>
        )}
      </Card>
    </Grid>
    <Grid type="columns">
      <Card onClick={changeTab('travel', activeTab, updateTab)} active={activeTab === 'travel'} inactive={activeTab && activeTab !=='travel'}>
        {data.totalTravellers ? (
          <Statistic
            label="Monthly No. of Travellers"
            icon="travellers"
            unit=""
            value={data.totalTravellers.value}
            monthlyChange={data.totalTravellers.monthlyChange}/>
        ): (
          <Loader/>
        )}
      </Card>
      <Card onClick={changeTab('trips', activeTab, updateTab)} active={activeTab === 'trips'} inactive={activeTab && activeTab !=='trips'}>
        {data.totalTrips ? (
          <Statistic
            label="Monthly No. of Trips"
            icon="flights"
            unit=""
            value={data.totalTrips.value}
            monthlyChange={data.totalTrips.monthlyChange}/>
        ): (
          <Loader/>
        )}
      </Card>
      <Card onClick={changeTab('accommodation', activeTab, updateTab)} active={activeTab === 'accommodation'} inactive={activeTab && activeTab !=='accommodation'}>
        {data.totalAccommodation ? (
          <Statistic
            label="Monthly No. of Nights Away"
            icon="accommodation"
            unit=""
            value={data.totalAccommodation.value}
            monthlyChange={data.totalAccommodation.monthlyChange}/>
        ): (
          <Loader/>
        )}
      </Card>
      <Card onClick={changeTab('cost', activeTab, updateTab)} active={activeTab === 'cost'} inactive={activeTab && activeTab !=='cost'}>
        {data.totalCost ? (
          <Statistic
            label="Monthly Cost of Travel"
            icon="cost"
            currency
            unit=""
            value={data.totalCost.value}
            monthlyChange={data.totalCost.monthlyChange}/>
        ): (
          <Loader/>
        )}
      </Card>
    </Grid>
    {activeTab &&
      <Fragment>
        {activeTab === 'trips' &&
          <Grid type="columns">
            <Trips tripData={data.totalTrips.detail} maps={data.totalTrips.maps}/>
          </Grid>
        }
        <Grid type="split">
          {activeTab === 'travel' &&
            <Fragment>
              <dl>
                <dt>Total no. of travellers this time last year</dt>
                <dd>{data.totalTravellers.previous}</dd>
              </dl>
              <Card large>
                <Chart legendPosition="right" data={data.totalTravellers.overview}/>
              </Card>
            </Fragment>
          }
          {activeTab === 'trips' &&
            <Fragment>
              <dl>
                <dt>Total no. of trips year to date</dt>
                <dd>{data.totalTrips.totalToDate} trips total</dd>
                <dt>Top three routes taken this month</dt>
                <dd>
                  <ol>
                  {data.totalTrips.routes.map((r,i) => (
                    <li key={i}>{r}</li>
                  ))}
                  </ol>
                </dd>
              </dl>
              <Card large>
                <Chart legendPosition="right" data={data.totalTrips.overview}/>
              </Card>
            </Fragment>
          }
          {activeTab === 'accommodation' &&
            <Fragment>
              <dl>
                <dt>No. of nights away year to date</dt>
                <dd>{data.totalAccommodation.totalToDate}</dd>
                <dt>No. of travellers staying away</dt>
                <dd>{data.totalAccommodation.travellers}</dd>
                <dd><Change {...data.totalAccommodation.change}/></dd>
                <dt>Reasons for nights away</dt>
                <dd>
                  <ol>
                    {data.totalAccommodation.reasons && data.totalAccommodation.reasons.map((r,i) => (
                      <li key={i}>{r}</li>
                    ))}
                  </ol>
                </dd>
                <dt>Top 5 Locations</dt>
                <dd>
                  <ol>
                    {data.totalAccommodation.locations && data.totalAccommodation.locations.map((l,i) => (
                      <li key={i}>{l}</li>
                    ))}
                  </ol>
                </dd>
              </dl>
              <Card large>
                <Chart legendPosition="right" data={data.totalAccommodation.overview}/>
              </Card>
            </Fragment>
          }
          {activeTab === 'cost' &&
            <Fragment>
              <dl>
                <dt>Reasons for travel</dt>
                <dd>
                  <ol>
                    {data.totalCost.reasons && data.totalCost.reasons.map((x,i) => (
                      <li key={i}>{x}</li>
                    ))}
                  </ol>
                </dd>
                <dt>Travel Breakdown</dt>
                <dd>
                  <ol>
                    {data.totalCost.breakdown && data.totalCost.breakdown.map((x,i) => (
                      <li key={i}>{x}</li>
                    ))}
                  </ol>
                </dd>
              </dl>
              <Card large>
                <Chart legendPosition="right" data={data.totalCost.overview}/>
              </Card>
            </Fragment>
          }
        </Grid>
      </Fragment>
    }
    {children}
  </Fragment>
};

export default DataView
