import React, { useState } from "react";
import styled from "styled-components";

const Snack = styled.div`
  & {
    transition-property: opacity, transform;
    transition-duration: 0.5s;
    transition-timing-function: ease;
    font-size: 14px;
    min-height: 14px;
    background-color: #323232;
    position: absolute;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: white;
    line-height: 22px;
    padding: 18px 24px;
    bottom: 0px;
    transform: translateY(100%);
    opacity: 0;
    ${props => props.active ? `{
      opacity: 1;
      transform: translateY(0);
    }` : ''}
  }

  @media (min-width: 640px) {
    & {
      min-width: 288px;
      max-width: 568px;
      display: inline-flex;
      border-radius: 2px;
      margin: 24px;      
    }
  }

  @media (max-width: 640px) {
    & {
      left: 0px;
      right: 0px;
    }
  }

  & p {
    margin: 0;
  }
`;

const SnackbarProvider = new (class {
  trigger(message, timeout) {
    this.snackTrigger && this.snackTrigger(message, timeout);
  }
})();

export const Snackbar = () => {

  const [message, setMessage] = useState(null);
  const [visibility, setVisibility] = useState(false);

  const triggerSnack = (message, timeout) => {
    setMessage(message);
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, timeout);
  };

  SnackbarProvider.snackTrigger = triggerSnack;

  return <Snack active={visibility}>
    <p>{message}</p>
  </Snack>;
}

export default SnackbarProvider;