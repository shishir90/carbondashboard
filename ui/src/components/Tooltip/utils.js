import { colors } from 'settings'

const arrowDirections = {
  right: `
    border-width: 10px 0 10px 10px;
    border-color: transparent transparent transparent ${colors.brandColor};
    left: 100%;
    top: 50%;
    transform: translateY(-50%);
  `,
  left: `
    border-width: 10px 10px 10px 0;
    border-color: transparent ${colors.brandColor} transparent transparent;
    right: 100%;
    top: 50%;
    transform: translateY(-50%);
  `,
  top: `
    border-width: 0 10px 10px 10px;
    border-color: transparent transparent ${colors.brandColor} transparent;
    bottom: 100%;
    left: 50%;
    transform: translateX(-50%);
  `,
  bottom: `
    border-width: 10px 10px 0 10px;
    border-color: ${colors.brandColor} transparent transparent transparent;
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
  `
}

const tooltipDirections = {
  right: `
    top: 50%; 
    transform: translateY(-50%);
    right: 50%;
  `,
  left: `
    top: 50%; 
    transform: translateY(-50%);
    left: 50%;
  `,
  top: `
    top: 100%;
    left: 50%; 
    transform: translateX(-50%);
  `,
  bottom: `
    bottom: 100%;
    left: 50%; 
    transform: translateX(-50%);
  `
}
  
const arrowDirection = direction => arrowDirections[direction]
const tooltipDirection = direction => tooltipDirections[direction]

export {
    arrowDirection,
    tooltipDirection
}