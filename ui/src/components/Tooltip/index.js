import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'settings'
import { arrowDirection, tooltipDirection } from './utils'

const Container = styled.span`
  font-size: 14px;
  color: #fff;
  background-color: ${colors.brandColor};
  position: absolute;
  border-radius: 8px;
  padding: 1em;
  max-width: 50%;
  box-sizing: border-box;
  opacity: 0;
  pointer-events: none;
  transition: opacity .3s;
  width: 100%;
  ${props => tooltipDirection(props.arrow)}
  ${props => props.visible &&
    'opacity: 1; pointer-events: auto;'
  }
  &::before {
    width: 0;
    height: 0;
    border-style: solid;
    position: absolute;
    content: "";
    ${props => arrowDirection(props.arrow)}
  }
`

const Tooltip = ({text, visible, arrow}) => (
  <Container visible={visible} arrow={arrow}>
    {text}
  </Container>
)

Tooltip.propTypes = {
  arrow: PropTypes.string,
  text: PropTypes.string.isRequired
}

Tooltip.defaultProps = {
    arrow: "left"
}

export default Tooltip