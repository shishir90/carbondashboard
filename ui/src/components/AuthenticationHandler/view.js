import React from "react";
export default ({ error, isAuthed, isLoading, authComponent, preAuthComponent }) => isAuthed ? authComponent : React.cloneElement(preAuthComponent, {
  error,
  isLoading
});