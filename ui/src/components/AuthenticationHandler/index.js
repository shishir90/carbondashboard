import React from "react";
import AuthenticationHandler from "./view";
import tokenStore from "authentication/tokenStore";

export default class extends React.Component {
  constructor() {
    super();
    this.state = {
      isAuthed: Boolean(process.env.REACT_APP_BYPASS_AUTH),
      isLoading: false,
      error: null
    };
  }
  componentDidMount() {
    // Is app auth token present?
    if (tokenStore.isAuthenticated() || this.state.isAuthed) {
      // Authenticated with Carbon API, render app. 
      this.setState({
        isAuthed: true
      });
    } else {
      // Show loading indicator. 
      this.setState({
        isLoading: true
      });
      // Fetch AD auth information if present
      fetch("/.auth/me")
      .then(res => res.json())
      .then(result => {
        if (result[0] && result[0].user_id) {
          // AD Auth is present, go through login flow.
          return fetch((process.env.REACT_APP_FUNCTIONS_EP || "") + "/api/login", {
            method: "POST",
            headers: {
              'Authorization': `Bearer ${result[0].id_token}`
            }
          })
          .then(response => {
            const getAuthUrl = function (renewTsAncCs) {
              // TODO if renewTsAbdCs then call api to update tsandcsdate value of user
              return "/.auth/login/aad?post_login_redirect_url=/";
            };
            switch (response.status) {
              case 200: {
                return response.json()
                  .then(response => {
                    if (!response.token) {
                      throw new Error("Authentication flow failed.");
                    } else {
                      tokenStore.token = response.token;
                      this.setState({
                        isAuthed: tokenStore.isAuthenticated(),
                        isLoading: false
                      }); 
                    }
                  });
              }
              case 403:
                return this.setState({
                  error: {
                    title: "You don't have access 😭",
                    message: "You don't currently have access to this tool, please request access from an account lead or the UK sustainability team.",
                    getAuthUrl: getAuthUrl
                  },
                  isLoading: false
                });
              case 412: {
                return this.setState({
                  error: {
                    title: "Your Ts&Cs confirmation has expired 😭",
                    message: "Lorem ipsum dolor sit amet, eum explicari definitionem te. Mea elitr sanctus et. Pro dico efficiantur id. Te modo graeci vidisse sit, est possim conclusionemque at, id eum dictas molestiae. Nec ad quot causae prompta, his id feugiat signiferumque. An sit utinam deserunt, ut prodesset moderatius theophrastus vim.",
                    getAuthUrl: getAuthUrl,
                    customBtnText: "Accept Ts and Cs"
                  },
                  isLoading: false
                })
              }
              default: {
                return response.json()
                  .then(body => {
                    if (body.error === "TokenExpiredError") {
                      return this.setState({
                        error: {
                          title: "Your login has expired 😭",
                          message: "We're having some issues logging you in, please try again later.",
                          getAuthUrl: getAuthUrl
                        },
                        isLoading: false
                      });      
                    } else {
                      throw new Error("Authentication flow failed");
                    }
                  })
              }
            }
          });
        } else {
          this.setState({
            isAuthed: tokenStore.isAuthenticated(),
            isLoading: false
          });
        }
      })
      .catch(e => {
        console.error(e);
        this.setState({
          isAuthed: tokenStore.isAuthenticated(),
          isLoading: false
        });
      });
    }
  }
  render() {
    return <AuthenticationHandler {...this.props} {...this.state} />
  }
}