import React from 'react'
import styled, {keyframes} from 'styled-components'

const shimmer = keyframes`
  from {
    background-position: -468px 0
  }
  to {
    background-position: 468px 0
  }
`

const Container = styled.div`
  max-width: 500px;
  box-shadow: inset 0 0 0 2px #ffffff;
  animation-duration: 1.25s;
  animation-fill-mode: forwards;
  animation-iteration-count: infinite;
  animation-name: ${shimmer};
  animation-timing-function: linear;
  background: #F6F6F6;
  background: linear-gradient(to right, #F6F6F6 8%, #F0F0F0 18%, #F6F6F6 33%);
  background-size: 800px 104px;
  position: relative;

  & img {
    display: block;
    width: 100%;
  }
`

const Loader = props => (
  <Container {...props}>
    <img src={require('images/loader.svg')} alt=""/>
  </Container>
)

export default Loader