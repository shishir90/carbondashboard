import React from "react";
import Dropzone from 'react-dropzone';
import styled from 'styled-components';

import Card from "components/Card";
import WindmillSpinner from 'components/Loaders/WindmillSpinner';

const Error = styled.p`
  color: #f00;
`;

const DropzoneWrapper = styled.div`
  & > div {
    width: 100%!important;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Loader = styled.div`
  & svg {
    position: relative;
    height: 200px;
    width: 200px;
  }
`;

export default ({ uploading, error, onDrop }) => {
  return <Card>
    <h2>Upload CSV</h2>
    <p>Upload new data into the Carbon Dashboard application.</p>
    {uploading && <Loader>
      <WindmillSpinner />
    </Loader>}
    {!uploading && <DropzoneWrapper>
      <Dropzone
        onDrop={onDrop}>
        <p>Try dropping some files here, or click to select files to upload.</p>
      </Dropzone>
    </DropzoneWrapper>}
    {error && <Error>{error}</Error>}
  </Card>;
};