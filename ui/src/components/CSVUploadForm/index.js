import React, { useState } from "react";
import { withRouter } from "react-router-dom";

import TokenStore from 'authentication/tokenStore';
import View from './view';
import uploadAPI from "api/upload";
import SnackbarProvider from "components/Snackbar";

export default withRouter(({ history }) => {
  const [state, setState] = useState({
    uploading: false,
    error: null
  });
  const sendFiles = (accepted, rejected) => {
    SnackbarProvider.trigger("Uploading your file... 💪", 3000);
    setState({
      uploading: true
    });
    uploadAPI.uploadFile(accepted[0])
      .then(() => {
        setState({
          uploading: false
        });
        SnackbarProvider.trigger("File successfully uploaded! 👍", 6000);
        history.push("/");
      })
      .catch(e => {
        console.error("Upload Error: ", e);
        setState({
          uploading: false,
          error: "Failed to upload, please refresh and retry."
        });
        SnackbarProvider.trigger("Failed to upload, please refresh and retry. 😭", 3000);
      })
  };
  const decoded = TokenStore.decoded;
  if (!decoded.role
    || !decoded.role.permissions
    || !decoded.role.permissions.data
    || !decoded.role.permissions.data.upload) {
    return null;
  }
  return <View {...state} onDrop={sendFiles} />;
});