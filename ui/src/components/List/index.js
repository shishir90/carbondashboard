import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const NumberedList = styled.ol`
    padding-inline-start: 0px;
    list-style-position: inside;
    line-height: 1.27;
    margin-top: 10px;
`;

const DefaultList = styled.ul`
    padding-inline-start: 0px;
    list-style-position: inside;
    line-height: 1.27;
    margin-top: 10px;
`;

const Items = ({data}) => data ? <React.Fragment>{data.map((value, key) => <li key={key}>{value}</li>)}</React.Fragment> : <span>-</span>;

const List = ({ type, data}) => {
    if (type === 'numbered') {
        return <NumberedList> <Items data={data}/> </NumberedList>;
    }
    return <DefaultList> <Items data={data}/> </DefaultList>;
}

List.defaultProps = {
    type: 'default'
};

List.propTypes = {
    type: PropTypes.oneOf(['numbered', 'default']),
    data: PropTypes.array.isRequired
};

export default List;