import React from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types';
import { colors } from 'settings';
import Dropdown from 'components/Dropdown';

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  & >* {
    flex: 1
  }
`;

const LeftContainer = styled.div``;

const Label = styled.div`
  height: 24px;
  margin: 0px;
  font-size: 21px;
  font-weight: 500;
  letter-spacing: 0.1px;
  color: #1a173b;
`

const Description = styled.div`
  height: 17px;
  font-size: 15px;
  letter-spacing: 0.3px;
  margin-top: 5px;
  color: ${colors.baseColorLight};
  text-transform: uppercase;
`

const Search = styled.input`
  outline: none;
  border: 1px solid ${colors.baseColorLight};
  font-size: 15px;
  padding: 10px 50px 10px 25px;
  background: url(${require('images/icons/magnifying-glass.svg')}) right center no-repeat;
  border-radius: 30px;
  height: 45px;
  box-sizing: border-box;
  width: 100%;
  max-width: 420px;
  margin: 0 20px;
  transition: border-color .2s;

  &:focus {
    border-color: ${colors.brandColor};
  }
`

const ProjectsHeader = ({count, title, description, sortOptions, sortBy, sortData, searchTerm, search, searchable}) => <Header>
  <LeftContainer>
    <Label>{title}</Label>
    <Description>{description}</Description>
  </LeftContainer>
  {searchable &&
    <Search value={searchTerm || ''} onChange={e => search(e.target.value)}/>
  }
  <Dropdown options={sortOptions} change={sortData} value={sortBy} label='sort by:'/>
</Header>;

ProjectsHeader.propTypes = {
  title: PropTypes.string.isRequired,
  sortBy: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  }).isRequired,
  sortOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired,
  searchable: PropTypes.bool
}

export default ProjectsHeader;
