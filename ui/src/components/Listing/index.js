import React, { useState, useEffect } from 'react';
import Header from './Header';
import List from './List';
import Card from 'components/Card';

const Projects = (props) => {
  let { count, dataRetrieval, list = [], title, sortOptions, fields, defaultSortBy, link, limit, paginate, searchable } = Object.assign({}, {
    paginate: {
      limit: 5
    },
    searchable: false
  }, props);

  let mappedFields = fields.map(f => ({ name: f.fieldName, resolveValue: f.resolveValue, label: f.title, type: list && list[0] && typeof f.resolveValue(list[0]), align: f.align }));
  let sortableFields = fields.filter(f => !!f.sortable).map(f => ({ value: f.fieldName, label: f.title, metric: f.metric }));

  defaultSortBy = sortableFields[sortableFields.length - 1];
  sortOptions = sortableFields

  const [sortBy, setSortBy] = useState(defaultSortBy);
  const [searchTerm, search] = useState(null);
  const [current, changePage] = useState(1);

  useEffect(() => {
    if (list && list.length > 0) {
      dataRetrieval((current - 1) * paginate.limit, paginate.limit, sortBy, searchTerm);
      dataRetrieval(current * paginate.limit, paginate.limit, sortBy, searchTerm);  
    }
  }, [current])

  useEffect(() => {
    dataRetrieval((current - 1) * paginate.limit, paginate.limit * 2, sortBy, searchTerm, true);
  }, [sortBy, searchTerm])


  const description = `TOP ${Math.min(paginate.limit, (list && list.length) || 0)} ${(sortBy && `BY ${sortBy.metric}`) || ""}`;

  // if ((data.length === 0 && list.length > 0) || !data || !list || data.length !== list.length) {
  //   setData(list);
  // }
  

  const sortData = (selected) => {
    // const value = selected.name;
    setSortBy(selected);
    changePage(1);
    // setData(list.sort((a,b) => (a[value] > b[value]) ? -1 : ((b[value] > a[value]) ? 1 : 0)));
  }
  return <Card>
    <Header count={count} title={title} description={description}  sortOptions={sortOptions} sortBy={sortBy} sortData={sortData} searchTerm={searchTerm} search={search} searchable={searchable}/>
    <List count={count} current={current} changePage={changePage} data={list} sortBy={sortBy.value} fields={mappedFields} link={link} limit={limit} paginate={paginate} searchTerm={searchTerm}/>
  </Card>;
}

export class Field {
  constructor(fieldName, title, sortMetric) {
    this.fieldName = fieldName;
    this.title = title;
    this.sortable = !!sortMetric;
    this.metric = sortMetric;
    this.align = this.sortable ? "center" : "left";
    this.resolveValue = this.resolveValue.bind(this);
  }
  resolveValue(object) {
    return this.fieldName.split(".")
      .reduce((value, pathSegment) => value && value[pathSegment], object);
  }
}

export default Projects;