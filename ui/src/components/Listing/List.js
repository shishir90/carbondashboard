import React, {Fragment, useState} from 'react';
import styled from 'styled-components'
import PropTypes from 'prop-types';
import Table from 'components/Table';
import Pagination from 'components/Pagination'
import {StyledButton} from 'constants/commonStyledItems'
import {colors} from 'settings'

const Limit = styled.div`
  text-align: center;
  position: relative;
  margin-top: 20px;
`

const Current = styled.p`
  position: absolute;
  right: 0;
  color: ${colors.baseColorLight};
  font-size: 14px;
  margin: 0;
  top: 0;
`

const pagination = (data, paginate, current) => {
  if (!data)
    data = [];
  const end = current * paginate.limit
  const start = end - (paginate.limit - 1)
  return {
    start,
    end,
    data: data.slice(start - 1, end)
  }
}

const ProjectsList = ({ count, current, changePage, data, sortBy, fields, link, limit, paginate, searchTerm}) => {
  const [expanded, expand] = useState(false)
  if (!data)
    data = [];
  const filteredData = pagination(data, paginate, current);
  return  (
    <Fragment>
      <Table data={filteredData.data} sortBy={sortBy} fields={fields} link={link} />
      {limit && !searchTerm &&
        <Limit>
          <StyledButton inactive onClick={() => expand(!expanded)}>{expanded ? 'Show less' : 'View all'} {limit.label}</StyledButton>
          <Current>Current View: {expanded ? data.length : limit.to} of {count}</Current>
        </Limit>
      }
      {paginate && !searchTerm &&
        <Pagination range={{
          start: filteredData.start,
          end: filteredData.end > data.length ? data.length : filteredData.end,
          total: count
        }} 
        total={filteredData.total}
        current={current}
        onChange={changePage}/>
      }
    </Fragment>
  )
};

ProjectsList.propTypes = {
  data: PropTypes.array,
  sortBy: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired,
  limit: PropTypes.shape({
    to: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired
  }),
  paginate: PropTypes.shape({
    limit: PropTypes.number.isRequired
  })
};

export default ProjectsList;
