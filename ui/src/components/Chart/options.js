import { colors } from 'settings'
const options = {
  textStyle: {
    color: '#6b7986',
    fontSize: 5,
    fontFamily: '"Ubuntu", "Helvetica Neue", sans-serif',
    fontWeight: 500
  },
  xAxis: {
    boundaryGap: false,
    data: ['', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC',''],
    axisLine: {
      lineStyle: {
        color: '#8a96a0'
      }
    },
    axisTick: {
      show: false
    },
    splitLine: {
      show: true
    },
    axisPointer: {
      show: true,
      triggerTooltip: false,
      lineStyle: {
        color: colors.error
      },
      status: 'show'
    },
  },
  yAxis: {
    type: 'value',
    axisLine: {
      lineStyle: {
        color: '#8a96a0'
      }
    },
    axisTick: {
      show: false
    },
    axisLabel: {
      formatter: '{value}K'
    }
  },
  grid: {
    right: 15,
    left: 0,
    top: 10,
    bottom: 0,
    borderColor: '#EAEDEE',
    containLabel: true
  },
  axisPointer: {
    triggerOn: 'none',
    label: {
      show: false
    }
  }
}

export default options