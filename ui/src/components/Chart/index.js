import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'settings'
import echarts from 'echarts'
import defaultChartOptions from './options'
import deepmerge from 'deepmerge'
import Checkbox from 'components/Checkbox'
import { StyledButton } from 'constants/commonStyledItems'

const Wrapper = styled.div`
  max-width: 800px;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1.5em;
`

const Title = styled.h3`
  font-size: 21px;
  font-weight: 500;
  margin: 0;
  color: ${colors.baseColor};
`

const Actions = styled.div`
  display: flex;
  align-items: center;
  & > * {
    margin-left: 1em;
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: ${props => props.legendPosition === 'bottom' ? 'column':'row'};

  @media (max-width: 1280px) {
    flex-direction: column;
  }
`

const Graph = styled.div`
  padding: 0 0 30px 30px;
  position: relative;
`

const Label = styled.p`
  font-size: 10px;
  text-transform: uppercase;
  color: #8a96a0;
  margin: 0;
  position: absolute;
  display: flex;
  align-items: center;
  ${props => props.axis === 'y' &&
    'transform: rotate(-90deg); transform-origin: bottom left; left: 10px; bottom: 45px;'
  }
  ${props => props.axis === 'x' &&
    'left: 65px; bottom: 0;'
  }
  & img {
    width: 74px;
    margin-left: 10px;
  }
`

const Legend = styled.div`
  margin-top: ${props => props.legendPosition === 'bottom' ? '1em':'0'};;
  display: flex;
  flex: 1;
  flex-direction: ${props => props.legendPosition === 'bottom' ? 'row':'column'};
  justify-content: ${props => props.legendPosition === 'bottom' ? 'flex-end':'center'};
  & > * {
    ${props => props.legendPosition === 'right' &&
      'margin-bottom: 10px;'
    }
  }
  @media (max-width: 1280px) {
    flex-direction: row;
    justify-content: flex-end;

    & > * {
      margin-bottom: 0;
    }
  }
`

class Chart extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: props.data,
      activeSet: props.data[0],
      hiddenSeries: props.data[0].chart.series.map((s,i) => s.indent ? `${s.name}-${i}` : null).filter(x => x),
      colors: ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3']
    }
  }
  generateTooltip = params => (`
    <div class="chart-tooltip" style="border-color: ${this.state.colors[params.componentIndex]}; color: ${this.state.colors[params.componentIndex]};">
      ${this.state.activeSet.tooltipFormat(params)}
      <div class="chart-tooltip__shadow" style="border-color: ${this.state.colors[params.componentIndex]};"></div>
      <div class="chart-tooltip__arrow"></div>
    </div>
  `)
  componentDidMount() {
    this.chart =  echarts.init(this.container)
    this.updateChart()
  }
  changeSet = setData => () => {
    this.setState({
      activeSet: setData,
      hiddenSeries: []
    }, this.updateChart)
    this.props.onActiveSetChange && this.props.onActiveSetChange(this.props.data.indexOf(setData));
  }
  toggleSeries = (series) => () => {
    const { hiddenSeries } = this.state
    this.setState({
      hiddenSeries: hiddenSeries.includes(series) ? hiddenSeries.filter((s,i) => s !== series) : [...hiddenSeries, series]
    }, this.updateChart)
  }
  updateChart = () => {
    const { activeSet, hiddenSeries, colors } = this.state
    const defaultOptions = {
      tooltip: {
        trigger: 'item',
        position: 'top',
        backgroundColor: 'transparent',
        formatter: this.generateTooltip
      },
      ...defaultChartOptions,
      yAxis: {
        ...defaultChartOptions.yAxis,
        axisLabel: {
          ...defaultChartOptions.yAxis.axisLabel,
          formatter: this.props.thousands ? '{value}K' : '{value}'
        }
      }
    }
    const series = activeSet.chart.series
      .map((s,i) => ({
        ...s, 
        smooth: true,
        lineStyle: { color: colors[i] , width: 1},
        itemStyle: { borderColor: colors[i] }
      }))
      .filter((s,i) => !hiddenSeries.includes(`${s.name}-${i}`))
    const chart = {
      ...activeSet.chart,
      series
    }
    this.chart.setOption(deepmerge(defaultOptions, chart), true)
  }
  componentWillReceiveProps(props) {
    if (!props || this.props === props)
      return;
    let newState = {
      data: props.data,
      hiddenSeries: props.data[0].chart.series.map((s,i) => s.indent ? `${s.name}-${i}` : null).filter(x => x),
      colors: ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3']
    };
    if (!this.state.activeSet) {
      newState.activeSet = props.data[0];
    }
    this.setState(newState, () => this.chart && this.updateChart());
  }
  render() {
    const { data, activeSet, colors, hiddenSeries } = this.state
    const { legendPosition } = this.props;
    return (
      <Wrapper>
        <Header>
          <Title>{activeSet.title}</Title>
          {data && data.length > 1 &&
            <Actions>
              {data.map(({title}, i) => (
                <StyledButton key={i} onClick={this.changeSet(data[i])} inactive={activeSet.title !== title}>{title}</StyledButton>
              ))}
            </Actions>
          }
        </Header>
        <Container legendPosition={legendPosition}>
          <Graph>
            <div ref={n => this.container = n} style={{width: 785, height: 325}}></div>
            <Label axis="y">{activeSet.yLabel} <img src={require('images/icons/graph-arrow.svg')} alt=""/></Label>
            <Label axis="x">{activeSet.xLabel} <img src={require('images/icons/graph-arrow.svg')} alt=""/></Label>
          </Graph>
          <Legend legendPosition={legendPosition}>
            {activeSet.chart.series.map((series, i) => (
              <Checkbox 
                indent={series.indent} 
                key={`${activeSet.title}-${series.name}-${i}`} 
                id={`${activeSet.title}-${series.name}-${i}`} 
                checked={!hiddenSeries.includes(`${series.name}-${i}`)} 
                onChange={this.toggleSeries(`${series.name}-${i}`)} 
                label={series.name} color={colors[i]}/>
            ))}
          </Legend>
        </Container>
      </Wrapper>
    )
  }
}

Chart.propTypes = {
  legendPosition: PropTypes.oneOf(['bottom', 'right']),
  data: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    xLabel: PropTypes.string.isRequired,
    yLabel: PropTypes.string.isRequired,
    tooltipFormat: PropTypes.func.isRequired,
    chart: PropTypes.object.isRequired
  }))
}

Chart.defaultProps = {
  legendPosition: 'bottom'
}

export default Chart